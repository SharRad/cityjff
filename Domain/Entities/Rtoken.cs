﻿using Domain.Common;

namespace Domain.Entities
{
    public class RToken : BaseEntity
    {
        public int UserId { get; set; }
        public string RefreshToken { get; set; }
        public bool IsStop { get; set; }
    }
}