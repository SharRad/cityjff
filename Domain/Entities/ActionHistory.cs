﻿using Domain.Common;
using Domain.Enums;
using System;

namespace Domain.Entities
{
    public class ActionHistory : BaseEntity
    {
        public int ComplaintId { get; set; }
        public CitizenComplaint CitizenComplaint { get; set; }
        public string UserTitle { get; set; }
        public string Description { get; set; }
        public CitizenComplaintStatus Status { get; set; }
        public DateTime ActionDate { get; set; }
        public bool IsRedirected { get; set; }
    }
}