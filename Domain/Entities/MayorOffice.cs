﻿using Domain.Common;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class MayorOffice : BaseDictionaryEntity
    {
        public MayorOffice()
        {
            CitizenComplaints = new List<CitizenComplaint>();
            StructuralUnits = new List<StructuralUnit>();
            ComplaintNotifications = new List<ComplaintNotification>();
        }

        public IList<CitizenComplaint> CitizenComplaints { get; set; }
        public IList<StructuralUnit> StructuralUnits { get; set; }
        public IList<ComplaintNotification> ComplaintNotifications { get; set; }
    }
}