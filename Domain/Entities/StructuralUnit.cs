﻿using Domain.Common;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class StructuralUnit : BaseDictionaryEntity
    {
        public StructuralUnit()
        {
            CitizenComplaintStructuralUnits = new List<CitizenComplaintStructuralUnit>();
            ComplaintNotifications = new List<ComplaintNotification>();
        }

        public int MayorOfficeId { get; set; }
        public MayorOffice MayorOffice { get; set; }

        public IList<CitizenComplaintStructuralUnit> CitizenComplaintStructuralUnits { get; set; }
        public IList<ComplaintNotification> ComplaintNotifications { get; set; }
    }
}