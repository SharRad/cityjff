﻿using Domain.Enums;

namespace Domain.Entities
{
    public class CitizenComplaintStructuralUnit
    {
        public int CitizenComplaintId { get; set; }
        public CitizenComplaint CitizenComplaint { get; set; }

        public int StructuralUnitId { get; set; }
        public StructuralUnit StructuralUnit { get; set; }

        public CitizenComplaintStatus Status { get; set; }

        public string Description { get; set; }
    }
}