﻿using Domain.Common;

namespace Domain.Entities
{
    public class NewsNotification : AuditableEntity
    {
        public string Title { get; set; }
        public string MessageBody { get; set; }
        public string Caption { get; set; }
        public string FileUrl { get; set; }
    }
}