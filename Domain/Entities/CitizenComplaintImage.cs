﻿using Domain.Common;

namespace Domain.Entities
{
    public class CitizenComplaintImage : BaseEntity
    {
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string FileExt { get; set; }
        public int CitizenComplaintId { get; set; }
        public CitizenComplaint CitizenComplaint { get; set; }
    }
}