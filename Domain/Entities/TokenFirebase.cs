﻿using Domain.Common;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class TokenFirebase : BaseEntity
    {
        public TokenFirebase()
        {
            CitizenComplaints = new List<CitizenComplaint>();
        }

        public int? UserId { get; set; }

        //public ApplicationUser User { get; set; }

        public string Token { get; set; }

        public bool NotifyEnabled { get; set; } = true;

        public List<CitizenComplaint> CitizenComplaints { get; set; }
    }
}