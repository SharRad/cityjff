﻿using Domain.Common;
using Domain.Enums;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class CitizenComplaint : AuditableEntity
    {
        public CitizenComplaint()
        {
            CitizenComplaintImages = new List<CitizenComplaintImage>();
            ActionHistories = new List<ActionHistory>();
            CitizenComplaintStructuralUnits = new List<CitizenComplaintStructuralUnit>();
            ComplaintNotifications = new List<ComplaintNotification>();
        }

        public CitizenComplaintStatus Status { get; set; }
        public string Description { get; set; }
        public int MayorOfficeId { get; set; }
        public MayorOffice MayorOffice { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string GeolocationImagePath { get; set; }

        public int TokenFirebaseId { get; set; }
        public TokenFirebase TokenFirebase { get; set; }

        public IList<CitizenComplaintImage> CitizenComplaintImages { get; set; }
        public IList<ActionHistory> ActionHistories { get; set; }
        public IList<CitizenComplaintStructuralUnit> CitizenComplaintStructuralUnits { get; set; }
        public IList<ComplaintNotification> ComplaintNotifications { get; set; }
    }
}