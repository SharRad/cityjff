﻿using Domain.Common;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class ComplaintNotification : BaseEntity
    {
        public ComplaintNotificationType NotificationType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int? MayorOfficeId { get; set; }
        public MayorOffice MayorOffice { get; set; }
        public int? StructuralUnitId { get; set; }
        public StructuralUnit StructuralUnit { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int CitizentComplaintId { get; set; }
        public CitizenComplaint CitizenComplaint { get; set; }
    }
}
