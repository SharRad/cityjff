﻿namespace Domain.Common
{
    public abstract class BaseDictionaryEntity : BaseEntity
    {
        public string Name { get; set; }

        public string NameKg { get; set; }
    }
}