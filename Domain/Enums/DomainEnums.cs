﻿namespace Domain.Enums
{
    public enum CitizenComplaintStatus
    {
        New = 1,
        InProgress = 2,
        ReadyToComplete = 3,
        Completed = 4,
        Canceled = 5
    }

    public enum UserStatus
    {
        Active = 1,
        Deleted = 2,
        Banned = 3
    }

    public enum Gender
    {
        Male = 1,
        Female = 2
    }

    public enum AccessControlLevel
    {
        Guest = -1,

        User = 10,

        Manager = 20,

        Administrator = 60
    }

    public enum ComplaintNotificationType
    {
        NewAdmin = 1,
        NewManager = 2,
        CanceledManager = 3
    }
}