﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Enums;
using System;

namespace Application.Identity.DTOs
{
    public class RoleDTO : IMapFrom<ApplicationRole>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public AccessControlLevel Acl { get; set; }
        public DateTime ActiveDateFrom { get; set; } = DateTime.Now;
        public DateTime? ActiveDateTo { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ApplicationRole, RoleDTO>();
            profile.CreateMap<RoleDTO, ApplicationRole>();
        }
    }
}