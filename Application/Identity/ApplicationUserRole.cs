﻿using Microsoft.AspNetCore.Identity;

namespace Application.Identity
{
    public class ApplicationUserRole : IdentityUserRole<int>
    {
        public override int RoleId { get; set; }
        public virtual ApplicationRole Role { get; set; }

        public override int UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}