﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Application.Identity
{
    public class ApplicationUser : IdentityUser<int>
    {
        public ApplicationUser()
        {
            UserRoles = new List<ApplicationUserRole>();
        }

        public bool IsRoot { get; set; }
        public UserStatus UserStatus { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public DateTime? BanEndDateTime { get; set; }
        public virtual IList<ApplicationUserRole> UserRoles { get; set; }
        public int? MayorOfficeId { get; set; }
        public virtual MayorOffice MayorOffice { get; set; }
        public int? StructuralUnitId { get; set; }
        public virtual StructuralUnit StructuralUnit { get; set; }
    }
}