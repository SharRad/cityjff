﻿using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Application.Identity
{
    public class ApplicationRole : IdentityRole<int>
    {
        public ApplicationRole()
        {
            UserRoles = new List<ApplicationUserRole>();
        }

        public bool IsActive { get; set; }
        public DateTime ActiveDateFrom { get; set; } = DateTime.Now;
        public DateTime? ActiveDateTo { get; set; }
        public AccessControlLevel Acl { get; set; }
        public virtual IList<ApplicationUserRole> UserRoles { get; set; }
    }
}