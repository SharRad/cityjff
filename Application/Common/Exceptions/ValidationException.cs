﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Common.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException() : base("One or more validation failures have occured...")
        {
            Errors = new Dictionary<string, string[]>();
        }

        public ValidationException(IEnumerable<ValidationFailure> failures) : this()
        {
            var failuresGroups = failures.GroupBy(e => e.PropertyName, e => e.ErrorMessage);

            foreach (var failuresGroup in failuresGroups)
            {
                var propertyName = failuresGroup.Key;
                var propertyFailures = failuresGroup.ToArray();

                Errors.Add(propertyName, propertyFailures);
            }
        }

        public IDictionary<string, string[]> Errors { get; }
    }
}