﻿namespace Application.Common.Models
{
    public class EmailSetting
    {
        public string FromEmailAddress { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool EnableSSL { get; set; }
    }
}