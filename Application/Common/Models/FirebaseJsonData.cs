﻿using Newtonsoft.Json;

namespace Application.Common.Models
{
    public class FirebaseJsonData
    {
        public FirebaseJsonData()
        {
            Title = null;
            ImagePath = null;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}