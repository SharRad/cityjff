﻿namespace Application.Common.Models
{
    public class EmailMessage
    {
        public string ToEmailAddress { get; }

        public string Subject { get; }

        public string Content { get; }

        public EmailMessage
            (string to, string subject, string content)
        {
            ToEmailAddress = to;
            Subject = subject;
            Content = content;
        }
    }
}