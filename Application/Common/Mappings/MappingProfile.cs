﻿using Application.Common.DTOs;
using Application.Identity;
using Application.MediatR.Admins.Queries.GetAdmins;
using Application.MediatR.Managers.Queries.GetManagers;
using AutoMapper;
using Domain.Entities;
using System;
using System.Linq;
using System.Reflection;

namespace Application.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());

            CreateMap<ApplicationRole, UserRoleDTO>();
            CreateMap<ActionHistoryDTO, ActionHistory>();
            CreateMap<ApplicationUser, ManagerDTO>();
            CreateMap<ApplicationUser, AdminDTO>();

            CreateMap<ApplicationUser, UserInfoDTO>()
                .ForMember(dest => dest.ACL, dest => dest.MapFrom(item => !item.IsRoot ? item.UserRoles.Select(x => x.Role).Min(x => x.Acl) : 0));
        }

        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var types = assembly.GetExportedTypes()
                .Where(t => t.GetInterfaces().Any(i =>
                    i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IMapFrom<>))).ToList();

            foreach (var type in types)
            {
                var instanse = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod("Mapping") ?? type.GetInterface("IMapFrom`1").GetMethod("Mapping");

                methodInfo?.Invoke(instanse, new object[] { this });
            }
        }
    }
}