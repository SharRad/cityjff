﻿using Domain.Enums;
using System.Collections.Generic;

namespace Application.Common.Helpers
{
    public static class StatusColorHelper
    {
        private static Dictionary<CitizenComplaintStatus, string> _colors = new Dictionary<CitizenComplaintStatus, string>
        {
            [CitizenComplaintStatus.New] = "#24C6C8",
            [CitizenComplaintStatus.InProgress] = "#1D84C6",
            [CitizenComplaintStatus.Completed] = "#1BB394",
            [CitizenComplaintStatus.Canceled] = "#ED5666",
            [CitizenComplaintStatus.ReadyToComplete] = "#F8AC5A"
        };

        public static string GetColor(CitizenComplaintStatus status) => _colors[status];
    }
}