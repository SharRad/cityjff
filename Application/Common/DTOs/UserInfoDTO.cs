﻿using Domain.Enums;

namespace Application.Common.DTOs
{
    public class UserAuthData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirebaseToken { get; set; }
    }

    public class UserRefreshToken
    {
        public string RefreshToken { get; set; }
    }

    public class ApiUserToken
    {
        public string JwtToken { get; set; }
        public string RtToken { get; set; }
    }

    public class UserInfoDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int? MayorOfficeId { get; set; }
        public int? StructuralUnitId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsRoot { get; set; }
        public AccessControlLevel ACL { get; set; }
    }

    public class UserRoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ChangePasswordDTO
    {
        public int UserId { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}