﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.Common.DTOs
{
    public class CitizenComplaintImageDto : IMapFrom<CitizenComplaintImage>
    {
        public int Id { get; set; }
        public string FileUrl { get; set; }
    }
}