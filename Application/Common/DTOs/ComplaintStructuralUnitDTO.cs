﻿using Application.MediatR.StructuralUnits.Queries.GetStructuralUnits;
using System.Collections.Generic;

namespace Application.Common.DTOs
{
    public class ComplaintStructuralUnitDTO
    {
        public int CitizenComplaintId { get; set; }
        public List<StructuralUnitDto> StructuralUnits { get; set; }
    }
}