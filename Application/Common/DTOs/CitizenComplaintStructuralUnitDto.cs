﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;

namespace Application.Common.DTOs
{
    public class CitizenComplaintStructuralUnitDto : IMapFrom<CitizenComplaintStructuralUnit>
    {
        public string StructuralUnitName { get; set; }
        public CitizenComplaintStatus Status { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CitizenComplaintStructuralUnit, CitizenComplaintStructuralUnitDto>()
                .ForMember(c => c.StructuralUnitName, opt => opt.MapFrom(c => c.StructuralUnit.Name));
        }
    }
}