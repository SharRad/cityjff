﻿using Application.Common.Mappings;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Common.DTOs
{
    public class CitizenComplaintDto : IMapFrom<CitizenComplaint>
    {
        public CitizenComplaintDto()
        {
            CitizenComplaintImages = new List<CitizenComplaintImageDto>();
            CitizenComplaintStructuralUnits = new List<CitizenComplaintStructuralUnitDto>();
        }

        public int Id { get; set; }
        public CitizenComplaintStatus Status { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string GeolocationImagePath { get; set; }

        public string UserEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Created { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public List<ActionHistoryDTO> ActionHistories { get; set; }
        public List<CitizenComplaintImageDto> CitizenComplaintImages { get; set; }
        public List<CitizenComplaintStructuralUnitDto> CitizenComplaintStructuralUnits { get; set; }

        public string GetCompletedPercentage()
        {
            if (CitizenComplaintStructuralUnits.Count > 0)
            {
                double count = CitizenComplaintStructuralUnits.Count(x => x.Status != CitizenComplaintStatus.Canceled);
                double completedCount = CitizenComplaintStructuralUnits.Count(x => x.Status == CitizenComplaintStatus.Completed);
                return $"{Math.Ceiling((completedCount / count) * 100)}%";
            }
            return "0%";
        }
    }
}