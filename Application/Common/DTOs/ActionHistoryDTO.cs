﻿using Application.Common.Mappings;
using Domain.Entities;
using Domain.Enums;
using System;

namespace Application.Common.DTOs
{
    public class ActionHistoryDTO : IMapFrom<ActionHistory>
    {
        public int Id { get; set; }
        public int ComplaintId { get; set; }
        public string UserTitle { get; set; }
        public string Description { get; set; }
        public CitizenComplaintStatus Status { get; set; }
        public DateTime ActionDate { get; set; }
        public bool IsRedirected { get; set; }
    }
}