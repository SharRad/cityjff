﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;
using System;

namespace Application.Common.DTOs
{
    public class CitizenComplaintShortDto : IMapFrom<CitizenComplaint>
    {
        public DateTime Created { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<CitizenComplaint, CitizenComplaintShortDto>()
                .ForMember(c => c.Status, opt => opt.MapFrom(c => c.Status.ToString()));
        }
    }
}