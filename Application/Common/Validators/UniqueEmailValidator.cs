﻿using Application.Common.Interfaces;
using Application.Identity;
using FluentValidation.Validators;

namespace Application.Common.Validators
{
    public class UniqueEmailValidator : PropertyValidator
    {
        private readonly IIdentityService _service;

        public UniqueEmailValidator(IIdentityService service) : base("'Email address' is already registered")
        {
            _service = service;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var emailAddress = context.PropertyValue as string;
            ApplicationUser user = _service.GetUserByEmail(emailAddress);
            return user == null;
        }
    }
}