﻿using System;

namespace Application.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToRuDateString(this DateTime dateTime)
        {
            return dateTime.ToString("F", new System.Globalization.CultureInfo("ru-Ru"));
        }
    }
}