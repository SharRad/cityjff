﻿using Application.Common.Models;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IRtTokenService
    {
        Task<(Result, string)> GenerateRToken(int userId);

        Task<(Result, string)> RefreshToken(int userId, string refreshToken);

        Task<Result> StopRefreshToken(int userId);
    }
}