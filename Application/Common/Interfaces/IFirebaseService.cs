﻿using Application.Common.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IFirebaseService
    {
        Task InsertUserFirebaseToken(int userId, string token);

        Task SendNotification(int userId, string title, string body, FirebaseJsonData jsonData);

        Task SendMultiNotifications(string title, string body, FirebaseJsonData jsonData);

        Task<bool> GetNotificationEnableStatus(string token, CancellationToken cancellationToken);

        Task<Result> SetNotificationEnableStatus(string token, bool enable);
    }
}