﻿using Microsoft.EntityFrameworkCore;

namespace Application.Common.Interfaces
{
    public interface ICustomExceptionHandler
    {
        string CatchDbException(DbUpdateException exception);
    }
}