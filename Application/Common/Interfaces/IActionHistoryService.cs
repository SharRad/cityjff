﻿using Application.Common.DTOs;
using Application.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IActionHistoryService
    {
        Task<Result> InsertActionHistory(ActionHistoryDTO dto);

        Task<Result> InsertActionHistories(List<ActionHistoryDTO> dtos);
    }
}