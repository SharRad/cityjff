﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IFileService
    {
        Task<string> AddFileAsync(IFormFile file);

        Task<List<string>> AddFilesAsync(IFormFileCollection files);

        string AddGeoFile(double latitude, double longitude);
    }
}