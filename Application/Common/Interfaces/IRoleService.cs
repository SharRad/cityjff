﻿using Application.Identity.DTOs;
using Domain.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IRoleService
    {
        Task<List<RoleDTO>> GetRoles();

        Task<RoleDTO> GetRoleByAcl(AccessControlLevel acl);
    }
}