﻿using Application.Common.DTOs;

namespace Application.Common.Interfaces
{
    public interface ICurrentUserService
    {
        UserInfoDTO Get { get; set; }
    }
}