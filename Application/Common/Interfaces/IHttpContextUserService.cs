﻿namespace Application.Common.Interfaces
{
    public interface IHttpContextUserService
    {
        public int UserId { get; }
    }
}