﻿using Application.Common.DTOs;
using Application.Common.Models;
using Application.Identity;
using Domain.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IIdentityService
    {
        Task<(Result, ApiUserToken)> Authenticate(UserAuthData userAuth);

        Task<Result> LoginAsync(string email, string password, bool rememberMe);

        Task SignoutAsync();

        Task<Result> ApiSignOutAsync(int userId);

        Task<(Result Result, int userId)> CreateUserAsync(ApplicationUser user, string password, AccessControlLevel acl);

        Task<(Result Result, int userId)> DeleteUserAsync(int userId);

        Task<List<ApplicationUser>> GetRootAdminsAsync();

        ApplicationUser GetUserByEmail(string email);

        Task<UserInfoDTO> GetUserByIdAsync(int id);

        Task<string> GetUserNameAsync(int userId);

        Task<Result> ChangePassword(ChangePasswordDTO dto);

        Task<(Result, string)> GenerateResetPasswordToken(string userMail);

        Task<Result> ResetUserPassword(string email, string code, string newPwd);

        Task<(Result, ApiUserToken)> RefreshToken(UserRefreshToken userRefreshData);

        Task<IList<ApplicationUser>> GetUsersByAclAsync(AccessControlLevel acl);

        Task<Result> BanAccount(int id);

        Task<Result> UnBanAccount(int id);

        Task<List<string>> GetMayorOfficeUserIds(int mayorOfficeId);
    }
}