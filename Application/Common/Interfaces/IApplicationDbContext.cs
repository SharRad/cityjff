﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<MayorOffice> MayorOffices { get; set; }

        DbSet<StructuralUnit> StructuralUnits { get; set; }

        DbSet<CitizenComplaint> CitizenComplaints { get; set; }
        DbSet<RToken> RTokens { get; set; }

        DbSet<CitizenComplaintStructuralUnit> ComplaintStructuralUnits { get; set; }

        DbSet<TokenFirebase> TokenFirebases { get; set; }

        DbSet<ActionHistory> ActionHistories { get; set; }

        DbSet<NewsNotification> NewsNotifications { get; set; }

        DbSet<ComplaintNotification> ComplaintNotifications { get; set; }

        Task<int> SaveChangesAsync(CancellationToken token);

        Task BeginTransactionAsync();

        Task CommitTransactionAsync();

        Task RollbackTransactionAsync();
    }
}