﻿using FluentValidation;

namespace Application.MediatR.MayorOffices.Commands.CreateMayorOffice
{
    public class CreateMayorOfficeCommandValidator : AbstractValidator<CreateMayorOfficeCommand>
    {
        public CreateMayorOfficeCommandValidator()
        {
            RuleFor(v => v.Name)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}