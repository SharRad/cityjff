﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.MayorOffices.Commands.CreateMayorOffice
{
    public class CreateMayorOfficeCommand : IRequest<Result>
    {
        public string Name { get; set; }
    }

    public class CreateMayorOfficeCommandHandler : IRequestHandler<CreateMayorOfficeCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CreateMayorOfficeCommandHandler> _logger;

        public CreateMayorOfficeCommandHandler(IApplicationDbContext context, ILogger<CreateMayorOfficeCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(CreateMayorOfficeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _context.MayorOffices.Add(new MayorOffice
                {
                    Name = request.Name
                });

                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"Mayor office creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}