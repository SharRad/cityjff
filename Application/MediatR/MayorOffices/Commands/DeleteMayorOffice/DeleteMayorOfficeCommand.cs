﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.MayorOffices.Commands.DeleteMayorOffice
{
    public class DeleteMayorOfficeCommand : IRequest<Result>
    {
        public int Id { get; set; }
    }

    public class DeleteMayorOfficeCommandHandler : IRequestHandler<DeleteMayorOfficeCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<DeleteMayorOfficeCommandHandler> _logger;

        public DeleteMayorOfficeCommandHandler(ILogger<DeleteMayorOfficeCommandHandler> logger, IApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<Result> Handle(DeleteMayorOfficeCommand request, CancellationToken cancellationToken)
        {
            var mayorOffice = await _context.MayorOffices.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (mayorOffice == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                _context.MayorOffices.Remove(mayorOffice);

                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"Mayor office remove failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.DeleteError);
            }
        }
    }
}