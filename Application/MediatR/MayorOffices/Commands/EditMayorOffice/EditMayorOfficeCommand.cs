﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.MayorOffices.Commands.EditMayorOffice
{
    public class EditMayorOfficeCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class EditMayorOfficeCommandHandler : IRequestHandler<EditMayorOfficeCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<EditMayorOfficeCommandHandler> _logger;

        public EditMayorOfficeCommandHandler(IApplicationDbContext context, ILogger<EditMayorOfficeCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(EditMayorOfficeCommand request, CancellationToken cancellationToken)
        {
            var mayorOffice = await _context.MayorOffices.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (mayorOffice == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                mayorOffice.Name = request.Name;

                _context.MayorOffices.Update(mayorOffice);
                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"Mayor office edit failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.EditError);
            }
        }
    }
}