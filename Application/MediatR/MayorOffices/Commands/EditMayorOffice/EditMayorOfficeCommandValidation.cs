﻿using FluentValidation;

namespace Application.MediatR.MayorOffices.Commands.EditMayorOffice
{
    public class EditMayorOfficeCommandValidation : AbstractValidator<EditMayorOfficeCommand>
    {
        public EditMayorOfficeCommandValidation()
        {
            RuleFor(v => v.Id).NotEmpty();
            RuleFor(v => v.Name)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}