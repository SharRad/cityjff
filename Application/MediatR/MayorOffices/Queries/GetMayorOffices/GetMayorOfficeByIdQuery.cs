﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.MayorOffices.Queries.GetMayorOffices
{
    public class GetMayorOfficeByIdQuery : IRequest<MayorOfficeDto>
    {
        public int Id { get; set; }
    }

    public class GetMayorOfficeByIdQueryHandler : IRequestHandler<GetMayorOfficeByIdQuery, MayorOfficeDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetMayorOfficeByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<MayorOfficeDto> Handle(GetMayorOfficeByIdQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<MayorOfficeDto>(_context.MayorOffices.AsNoTracking())
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        }
    }
}