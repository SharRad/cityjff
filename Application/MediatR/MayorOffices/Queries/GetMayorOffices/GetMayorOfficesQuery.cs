﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.MayorOffices.Queries.GetMayorOffices
{
    public class GetMayorOfficesQuery : IRequest<List<MayorOfficeDto>>
    {
    }

    public class GetMayorOfficesQueryHandler : IRequestHandler<GetMayorOfficesQuery, List<MayorOfficeDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetMayorOfficesQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<List<MayorOfficeDto>> Handle(GetMayorOfficesQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<MayorOfficeDto>(_context.MayorOffices.AsNoTracking())
                .ToListAsync(cancellationToken);
        }
    }
}