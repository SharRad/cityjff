﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.MediatR.MayorOffices.Queries.GetMayorOffices
{
    public class MayorOfficeDto : IMapFrom<MayorOffice>
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}