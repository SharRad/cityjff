﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.ComplaintNotifications.Commands.CreateComplaintNotification
{
    public class CreateNewComplaintNotificationCommand : IRequest<Result>
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public int? MayorOfficeId { get; set; }
        public int? StructuralUnitId { get; set; }
        public int CitizentComplaintId { get; set; }
    }


    public class CreateNewComplaintNotificationCommandHandler : IRequestHandler<CreateNewComplaintNotificationCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CreateNewComplaintNotificationCommandHandler> _logger;
        private readonly IDateTime _dateTimeService;

        public CreateNewComplaintNotificationCommandHandler(IApplicationDbContext context, IDateTime dateTimeService,
            ILogger<CreateNewComplaintNotificationCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
            _dateTimeService = dateTimeService;
        }

        public async Task<Result> Handle(CreateNewComplaintNotificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _context.ComplaintNotifications.Add(new ComplaintNotification
                {
                    NotificationType = ComplaintNotificationType.NewAdmin,
                    Title = request.Title,
                    Message = request.Message,
                    MayorOfficeId = request.MayorOfficeId,
                    StructuralUnitId = request.StructuralUnitId,
                    CreatedDateTime = _dateTimeService.Now,
                    CitizentComplaintId = request.CitizentComplaintId
                });

                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Complaint notification creation failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}
