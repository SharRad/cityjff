﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.ComplaintNotifications.Commands.DeleteComplaintNotification
{
    public class DeleteComplaintNotificationCommand : IRequest<Result>
    {
        public int Id { get; set; }
    }

    public class DeleteComplaintNotificationCommandHandler : IRequestHandler<DeleteComplaintNotificationCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<DeleteComplaintNotificationCommandHandler> _logger;

        public DeleteComplaintNotificationCommandHandler(IApplicationDbContext context, ILogger<DeleteComplaintNotificationCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(DeleteComplaintNotificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var complaintNotification = await _context.ComplaintNotifications.SingleOrDefaultAsync(c => c.Id == request.Id, cancellationToken);
                
                if (complaintNotification == null)
                    return Result.Failure(ApplicationResources.DeleteError);

                _context.ComplaintNotifications.Remove(complaintNotification);
                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Complaint notification delete failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.DeleteError);
            }
        }
    }
}
