﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.ComplaintNotifications.Commands.DeleteComplaintNotification
{
    public class DeleteManagerNewComplaintNotificationsCommand : IRequest<Result>
    {
    }

    public class DeleteManagerNewComplaintNotificationsCommandHandler : IRequestHandler<DeleteManagerNewComplaintNotificationsCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<DeleteAdminNewComplaintNotificationsCommandHandler> _logger;
        private readonly ICurrentUserService _userService;

        public DeleteManagerNewComplaintNotificationsCommandHandler(IApplicationDbContext context, ICurrentUserService userService,
            ILogger<DeleteAdminNewComplaintNotificationsCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
            _userService = userService;
        }


        public async Task<Result> Handle(DeleteManagerNewComplaintNotificationsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var complaintNotifications = await _context.ComplaintNotifications
                    .Where(c => c.NotificationType == ComplaintNotificationType.NewManager && c.StructuralUnitId == _userService.Get.StructuralUnitId)
                    .ToListAsync(cancellationToken);

                if (!complaintNotifications.Any())
                    return Result.Failure(ApplicationResources.DeleteError);

                _context.ComplaintNotifications.RemoveRange(complaintNotifications);
                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"New manager complaint notifications delete failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.DeleteError);
            }
        }
    }
}
