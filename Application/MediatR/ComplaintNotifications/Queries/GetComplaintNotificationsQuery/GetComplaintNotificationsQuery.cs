﻿using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.ComplaintNotifications.Queries.GetComplaintNotificationsQuery
{
    public class GetComplaintNotificationsQuery : IRequest<List<ComplaintNotificationDto>>
    {
    }

    public class GetComplaintNotificationsQueryHandler : IRequestHandler<GetComplaintNotificationsQuery, List<ComplaintNotificationDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly ICurrentUserService _userService;
        private readonly IMapper _mapper;

        public GetComplaintNotificationsQueryHandler(IApplicationDbContext context, ICurrentUserService userService, IMapper mapper)
        {
            _context = context;
            _userService = userService;
            _mapper = mapper;
        }

        public Task<List<ComplaintNotificationDto>> Handle(GetComplaintNotificationsQuery request, CancellationToken cancellationToken)
        {
            return _context.ComplaintNotifications
                .Where(c => c.MayorOfficeId == _userService.Get.MayorOfficeId && c.NotificationType == ComplaintNotificationType.CanceledManager)
                .ProjectTo<ComplaintNotificationDto>(_mapper.ConfigurationProvider)
                .AsNoTracking()
                .ToListAsync(cancellationToken);
        }
    }
}
