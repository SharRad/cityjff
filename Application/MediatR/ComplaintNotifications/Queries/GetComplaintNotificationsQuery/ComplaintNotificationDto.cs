﻿using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MediatR.ComplaintNotifications.Queries.GetComplaintNotificationsQuery
{
    public class ComplaintNotificationDto : IMapFrom<ComplaintNotification>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int CitizentComplaintId { get; set; }
    }
}
