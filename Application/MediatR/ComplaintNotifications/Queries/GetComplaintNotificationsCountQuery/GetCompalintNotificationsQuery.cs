﻿using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.ComplaintNotifications.Queries.GetComplaintNotificationsCountQuery
{
    public class GetCompalintNotificationsCountQuery : IRequest<ComplaintNotificationVm>
    {
    }

    public class GetCompalintNotificationsQueryHandler : IRequestHandler<GetCompalintNotificationsCountQuery, ComplaintNotificationVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly ICurrentUserService _userService;

        public GetCompalintNotificationsQueryHandler(IApplicationDbContext context, ICurrentUserService userService)
        {
            _context = context;
            _userService = userService;
        }


        public async Task<ComplaintNotificationVm> Handle(GetCompalintNotificationsCountQuery request, CancellationToken cancellationToken)
        {
            var currentUser = _userService.Get;

            var complainsNotificationsVm = new ComplaintNotificationVm();

            if (currentUser.StructuralUnitId.HasValue)
            {
                complainsNotificationsVm.IsAdministrator = false;
                complainsNotificationsVm.NewComplaintsCount = await _context.ComplaintNotifications
                    .CountAsync(c => c.StructuralUnitId == currentUser.StructuralUnitId, cancellationToken);
            }
            else if (currentUser.MayorOfficeId.HasValue)
            {
                complainsNotificationsVm.IsAdministrator = true;
                complainsNotificationsVm.NewComplaintsCount = await _context.ComplaintNotifications
                    .CountAsync(c => c.MayorOfficeId == currentUser.MayorOfficeId && c.NotificationType == ComplaintNotificationType.NewAdmin, cancellationToken);
                complainsNotificationsVm.CanceledComplaintsCount = await _context.ComplaintNotifications
                    .CountAsync(c => c.MayorOfficeId == currentUser.MayorOfficeId && c.NotificationType == ComplaintNotificationType.CanceledManager, cancellationToken);
            }
            else
            {
                complainsNotificationsVm.IsAdministrator = false;
                complainsNotificationsVm.NewComplaintsCount = 0;
            }

            return complainsNotificationsVm;
        }
    }
}
