﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MediatR.ComplaintNotifications.Queries.GetComplaintNotificationsCountQuery
{
    public class ComplaintNotificationVm
    {
        public bool IsAdministrator { get; set; }
        public int NewComplaintsCount { get; set; }
        public int CanceledComplaintsCount { get; set; }
        public int TotalComplaintsCount => NewComplaintsCount + CanceledComplaintsCount;
    }
}
