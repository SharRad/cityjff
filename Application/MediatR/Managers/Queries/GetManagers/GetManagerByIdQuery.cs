﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Managers.Queries.GetManagers
{
    public class GetManagerByIdQuery : IRequest<ManagerDTO>
    {
        public int Id { get; set; }
    }

    public class GetManagerByIdQueryHandler : IRequestHandler<GetManagerByIdQuery, ManagerDTO>
    {
        private readonly IIdentityService _identityService;
        private readonly IMapper _mapper;

        public GetManagerByIdQueryHandler(IIdentityService identityService, IMapper mapper)
        {
            _identityService = identityService;
            _mapper = mapper;
        }

        public async Task<ManagerDTO> Handle(GetManagerByIdQuery request, CancellationToken cancellationToken)
        {
            return _mapper.Map<ManagerDTO>(await _identityService.GetUserByIdAsync(request.Id));
        }
    }
}