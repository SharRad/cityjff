﻿using Domain.Enums;

namespace Application.MediatR.Managers.Queries.GetManagers
{
    public class ManagerDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserStatus UserStatus { get; set; }
    }
}