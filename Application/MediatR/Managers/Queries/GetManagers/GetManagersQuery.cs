﻿using Application.Common.Interfaces;
using AutoMapper;
using Domain.Enums;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Managers.Queries.GetManagers
{
    public class GetManagersQuery : IRequest<List<ManagerDTO>>
    {
    }

    public class GetManagersQueryHandler : IRequestHandler<GetManagersQuery, List<ManagerDTO>>
    {
        private readonly IIdentityService _service;
        private readonly IMapper _mapper;

        public GetManagersQueryHandler(IIdentityService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<List<ManagerDTO>> Handle(GetManagersQuery request, CancellationToken cancellationToken)
        {
            var users = await _service.GetUsersByAclAsync(AccessControlLevel.Manager);
            return _mapper.Map<List<ManagerDTO>>(users);
        }
    }
}