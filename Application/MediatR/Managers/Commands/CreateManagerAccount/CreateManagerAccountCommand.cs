﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Identity;
using Domain.Enums;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Managers.Commands.CreateManagerAccount
{
    public class CreateManagerAccountCommand : IRequest<Result>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int MayorOfficeId { get; set; } = 1;
        public int StructuralUnitId { get; set; }
    }

    public class CreateManagerAccountCommandHandler : IRequestHandler<CreateManagerAccountCommand, Result>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<CreateManagerAccountCommandHandler> _logger;

        public CreateManagerAccountCommandHandler(IIdentityService identityService, ILogger<CreateManagerAccountCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<Result> Handle(CreateManagerAccountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var manager = new ApplicationUser
                {
                    UserName = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    UserStatus = UserStatus.Active,
                    IsRoot = false,
                    MayorOfficeId = request.MayorOfficeId,
                    StructuralUnitId = request.StructuralUnitId
                };

                var (Result, userId) = await _identityService.CreateUserAsync(manager, request.Password, AccessControlLevel.Manager);

                if (!Result.Succeeded)
                {
                    var exception = new Exception(Result.Errors.ToString());
                    throw exception;
                }
                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{GetType().Name} failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}