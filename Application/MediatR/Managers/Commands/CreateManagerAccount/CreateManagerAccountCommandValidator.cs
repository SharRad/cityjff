﻿using Application.Common.Interfaces;
using Application.Common.Validators;
using FluentValidation;

namespace Application.MediatR.Managers.Commands.CreateManagerAccount
{
    public class CreateManagerAccountCommandValidator : AbstractValidator<CreateManagerAccountCommand>
    {
        public CreateManagerAccountCommandValidator(IIdentityService service)
        {
            RuleFor(v => v.Email)
                .MaximumLength(50).WithMessage(ApplicationResources.MaxLengthValidation)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .EmailAddress()
                .SetValidator(new UniqueEmailValidator(service));

            RuleFor(c => c.FirstName)
                .MaximumLength(50).WithMessage(ApplicationResources.MaxLengthValidation)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);

            RuleFor(c => c.LastName)
                .MaximumLength(50).WithMessage(ApplicationResources.MaxLengthValidation)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);

            RuleFor(c => c.Password)
                .MinimumLength(6).WithMessage(ApplicationResources.MinimumLengthValidation)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);

            RuleFor(c => c.ConfirmPassword)
                .MinimumLength(6).WithMessage(ApplicationResources.MinimumLengthValidation)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .Equal(x => x.Password).WithMessage(ApplicationResources.PasswordConfirm);

            RuleFor(c => c.MayorOfficeId)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);

            RuleFor(c => c.StructuralUnitId)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}