﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Maps.Queries.GetStructureMapComplaint
{
    public class GetStructureMapComplaintQuery : IRequest<MapComplaintDTO>
    {
    }

    public class GetUserMapComplaintQueryHandler : IRequestHandler<GetStructureMapComplaintQuery, MapComplaintDTO>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetUserMapComplaintQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<MapComplaintDTO> Handle(GetStructureMapComplaintQuery request, CancellationToken cancellationToken)
        {
            MapComplaintDTO mapComplaint = new MapComplaintDTO();
            var features = await _context.CitizenComplaints.Select(x => new Feature
            {
                Properties = new Properties
                {
                    Id = x.Id,
                    Description = x.Description,
                    Status = x.Status,
                    BackgroundColor = StatusColorHelper.GetColor(x.Status)
                },
                Geometry = new Geometry
                {
                    Coordinates = new double[] { x.Longitude, x.Latitude }
                }
            }).ToListAsync();
            mapComplaint.Features.AddRange(features);
            return mapComplaint;
        }
    }
}