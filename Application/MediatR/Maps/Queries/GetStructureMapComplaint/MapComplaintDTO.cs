﻿using Domain.Enums;
using System.Collections.Generic;

namespace Application.MediatR.Maps.Queries.GetStructureMapComplaint
{
    public class MapComplaintDTO
    {
        public MapComplaintDTO()
        {
            Features = new List<Feature>();
            Type = "FeatureCollection";
        }

        public string Type { get; set; }
        public List<Feature> Features { get; set; }
    }

    public class Feature
    {
        public Feature()
        {
            Type = "Feature";
        }

        public string Type { get; set; } = "Feature";
        public Properties Properties { get; set; }
        public Geometry Geometry { get; set; }
    }

    public class Properties
    {
        public int Id { get; set; }
        public CitizenComplaintStatus Status { get; set; }
        public string BackgroundColor { get; set; }
        public string Description { get; set; }
    }

    public class Geometry
    {
        public Geometry()
        {
            Type = "Point";
        }

        public string Type { get; set; } = "Point";
        public double[] Coordinates { get; set; }
    }
}