﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Commands.DeleteNewsNotification
{
    public class DeleteNewsNotificationCommand : IRequest<Result>
    {
        public int Id { get; set; }
    }

    public class DeleteNewsNotificationCommandHandler : IRequestHandler<DeleteNewsNotificationCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<DeleteNewsNotificationCommandHandler> _logger;

        public DeleteNewsNotificationCommandHandler(ILogger<DeleteNewsNotificationCommandHandler> logger, IApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<Result> Handle(DeleteNewsNotificationCommand request, CancellationToken cancellationToken)
        {
            var newsNotification = await _context.NewsNotifications.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (newsNotification == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                _context.NewsNotifications.Remove(newsNotification);
                await _context.SaveChangesAsync(cancellationToken);
                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"News Notification remove failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.DeleteError);
            }
        }
    }
}