﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Commands.UpdateNotificationEnableStatus
{
    public class UpdateNotificationEnableStatusCommand : IRequest<Result>
    {
        public string Token { get; set; }
        public bool Enable { get; set; }
    }

    public class UpdateNotificationEnableStatusCommandHandler : IRequestHandler<UpdateNotificationEnableStatusCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<UpdateNotificationEnableStatusCommandHandler> _logger;

        public UpdateNotificationEnableStatusCommandHandler(IApplicationDbContext context, ILogger<UpdateNotificationEnableStatusCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(UpdateNotificationEnableStatusCommand request, CancellationToken cancellationToken)
        {
            var firebaseToken = await _context.TokenFirebases.SingleOrDefaultAsync(x => x.Token == request.Token, cancellationToken);

            if (firebaseToken == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                firebaseToken.NotifyEnabled = request.Enable;

                _context.TokenFirebases.Update(firebaseToken);
                await _context.SaveChangesAsync(cancellationToken);
                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"Token firebase edit failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.EditError);
            }
        }
    }
}
