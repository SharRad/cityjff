﻿using FluentValidation;

namespace Application.MediatR.NewsNotifications.Commands.EditNewsNotification
{
    public class EditNewsNotificationCommandValidator : AbstractValidator<EditNewsNotificationCommand>
    {
        public EditNewsNotificationCommandValidator()
        {
            RuleFor(v => v.Id).NotEmpty();
            RuleFor(v => v.Title)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
            RuleFor(v => v.MessageBody)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.Caption)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}