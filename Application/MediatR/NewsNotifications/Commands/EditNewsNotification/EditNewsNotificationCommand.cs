﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Commands.EditNewsNotification
{
    public class EditNewsNotificationCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string MessageBody { get; set; }
        public string Caption { get; set; }
        public string FileUrl { get; set; }
    }

    public class EditNewsNotificationCommandHandler : IRequestHandler<EditNewsNotificationCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<EditNewsNotificationCommandHandler> _logger;

        public EditNewsNotificationCommandHandler(IApplicationDbContext context, ILogger<EditNewsNotificationCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(EditNewsNotificationCommand request, CancellationToken cancellationToken)
        {
            var newsNotification = await _context.NewsNotifications.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (newsNotification == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                newsNotification.Title = request.Title;
                newsNotification.MessageBody = request.MessageBody;
                newsNotification.Caption = request.Caption;

                _context.NewsNotifications.Update(newsNotification);
                await _context.SaveChangesAsync(cancellationToken);
                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"News Notifications edit failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.EditError);
            }
        }
    }
}