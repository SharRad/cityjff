﻿using FluentValidation;

namespace Application.MediatR.NewsNotifications.Commands.CreateNewsNotification
{
    public class CreateNewsNotificationCommandValidator : AbstractValidator<CreateNewsNotificationCommand>
    {
        public CreateNewsNotificationCommandValidator()
        {
            RuleFor(v => v.Title)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
            RuleFor(v => v.MessageBody)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.Caption)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}