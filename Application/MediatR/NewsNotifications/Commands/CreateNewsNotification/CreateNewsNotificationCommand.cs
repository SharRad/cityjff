﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Commands.CreateNewsNotification
{
    public class CreateNewsNotificationCommand : IRequest<Result>
    {
        public string Title { get; set; }
        public string MessageBody { get; set; }
        public string Caption { get; set; }
        public string FileUrl { get; set; }
    }

    public class CreateNewsNotificationCommandHandler : IRequestHandler<CreateNewsNotificationCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly IFirebaseService _firebaseService;
        private readonly ILogger<CreateNewsNotificationCommandHandler> _logger;

        public CreateNewsNotificationCommandHandler
            (IApplicationDbContext context, ILogger<CreateNewsNotificationCommandHandler> logger,
            IFirebaseService firebaseService)
        {
            _context = context;
            _firebaseService = firebaseService;
            _logger = logger;
        }

        public async Task<Result> Handle(CreateNewsNotificationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _context.NewsNotifications.Add(new NewsNotification
                {
                    Title = request.Title,
                    MessageBody = request.MessageBody,
                    Caption = request.Caption
                });
                await _context.SaveChangesAsync(cancellationToken);
                await _firebaseService.SendMultiNotifications(request.Title, request.MessageBody, new FirebaseJsonData());
                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"News Notifications creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}