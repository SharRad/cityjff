﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.MediatR.NewsNotifications.Queries.GetNewsNotifications
{
    public class NewsNotificationDto : IMapFrom<NewsNotification>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string MessageBody { get; set; }
        public string Caption { get; set; }
    }
}