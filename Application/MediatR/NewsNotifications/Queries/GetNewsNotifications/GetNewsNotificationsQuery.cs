﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Queries.GetNewsNotifications
{
    public class GetNewsNotificationsQuery : IRequest<IPager<NewsNotificationDto>>
    {
        public int Page { get; set; }
    }

    public class GetNewsNotificationsQueryHandler : IRequestHandler<GetNewsNotificationsQuery, IPager<NewsNotificationDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetNewsNotificationsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<IPager<NewsNotificationDto>> Handle(GetNewsNotificationsQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<NewsNotificationDto>(_context.NewsNotifications.AsNoTracking().OrderByDescending(x => x.Created))
                .ToPagerListAsync(request.Page, 20, cancellationToken);
        }
    }
}
