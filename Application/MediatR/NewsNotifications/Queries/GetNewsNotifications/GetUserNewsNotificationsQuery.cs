﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Queries.GetNewsNotifications
{
    public class GetUserNewsNotificationsQuery : IRequest<IPager<NewsNotificationDto>>
    {
        public int Page { get; set; }
    }

    public class GetUserNewsNotificationsQueryHandler : IRequestHandler<GetUserNewsNotificationsQuery, IPager<NewsNotificationDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IHttpContextUserService _userService;
        private readonly IMapper _mapper;

        public GetUserNewsNotificationsQueryHandler(IApplicationDbContext context, IMapper mapper, IHttpContextUserService userService)
        {
            _context = context;
            _userService = userService;
            _mapper = mapper;
        }

        public Task<IPager<NewsNotificationDto>> Handle(GetUserNewsNotificationsQuery request, CancellationToken cancellationToken)
        {
            int userId = _userService.UserId;
            return _mapper.ProjectTo<NewsNotificationDto>(_context.NewsNotifications.AsNoTracking().Where(c => c.CreatedBy == userId).OrderByDescending(x => x.Created))
                .ToPagerListAsync(request.Page, 20, cancellationToken);
        }
    }
}