﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Queries.GetNewsNotifications
{
    public class GetNewsNotificationByIdQuery : IRequest<NewsNotificationDto>
    {
        public int Id { get; set; }
    }

    public class GetNewsNotificationByIdQueryHandler : IRequestHandler<GetNewsNotificationByIdQuery, NewsNotificationDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetNewsNotificationByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<NewsNotificationDto> Handle(GetNewsNotificationByIdQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<NewsNotificationDto>(_context.NewsNotifications.AsNoTracking())
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        }
    }
}