﻿using Application.Common.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.NewsNotifications.Queries.GetNotificationEnableStatus
{
    public class GetNotificationEnableStatusQuery : IRequest<bool>
    {
        public string Token { get; set; }
    }
    public class GetNotificationEnableStatusQueryHandler : IRequestHandler<GetNotificationEnableStatusQuery, bool>
    {
        private readonly IFirebaseService _service;

        public GetNotificationEnableStatusQueryHandler(IFirebaseService service)
        {
            _service = service;
        }

        public Task<bool> Handle(GetNotificationEnableStatusQuery request, CancellationToken cancellationToken)
        {
            return _service.GetNotificationEnableStatus(request.Token, cancellationToken);
        }
    }
}
