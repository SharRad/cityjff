﻿using FluentValidation;

namespace Application.MediatR.DivisionCitizenComplaints.Commands.CancelDivisionComplaint
{
    public class CancelDivisionComplaintCommandValidator : AbstractValidator<CancelDivisionComplaintCommand>
    {
        public CancelDivisionComplaintCommandValidator()
        {
            RuleFor(v => v.Description)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}