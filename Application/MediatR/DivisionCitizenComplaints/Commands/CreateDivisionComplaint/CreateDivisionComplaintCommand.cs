﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.DivisionCitizenComplaints.Commands.CreateDivisionComplaint
{
    public class CreateDivisionComplaintCommand : IRequest<Result>
    {
        public int CitizenComplaintId { get; set; }
        public List<int> StructuralUnitIds { get; set; }
    }

    public class CreateDivisionComplaintCommandHandler : IRequestHandler<CreateDivisionComplaintCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CreateDivisionComplaintCommandHandler> _logger;
        private readonly IActionHistoryService _actionHistoryService;
        private readonly IDateTime _dateTime;

        public CreateDivisionComplaintCommandHandler
            (IApplicationDbContext context, ILogger<CreateDivisionComplaintCommandHandler> logger,
            IActionHistoryService actionHistoryService, IDateTime dateTime)
        {
            _context = context;
            _logger = logger;
            _actionHistoryService = actionHistoryService;
            _dateTime = dateTime;
        }

        public async Task<Result> Handle(CreateDivisionComplaintCommand request, CancellationToken cancellationToken)
        {
            if (request.StructuralUnitIds == null)
                return Result.Failure(ApplicationResources.ListRequired);

            var citizenComplaint = await _context.CitizenComplaints.SingleOrDefaultAsync(x => x.Id == request.CitizenComplaintId, cancellationToken);
            if (citizenComplaint == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            var structuralUnits = await _context.StructuralUnits.Where(x => x.MayorOfficeId == 1).ToListAsync(cancellationToken);
            try
            {
                citizenComplaint.Status = CitizenComplaintStatus.InProgress;
                ActionHistoryDTO historyDTO = new ActionHistoryDTO
                {
                    ActionDate = _dateTime.Now,
                    Status = CitizenComplaintStatus.InProgress,
                    ComplaintId = citizenComplaint.Id,
                    Description = "Заявка принята в Мэрию.",
                    UserTitle = "Мэрия Бишкек"
                };
                await _context.BeginTransactionAsync();
                await _actionHistoryService.InsertActionHistory(historyDTO);
                _context.CitizenComplaints.Update(citizenComplaint);

                List<CitizenComplaintStructuralUnit> citizenComplaintStructurals = new List<CitizenComplaintStructuralUnit>();
                List<ActionHistoryDTO> actionHistories = new List<ActionHistoryDTO>();
                foreach (var item in request.StructuralUnitIds)
                {
                    AddItemsToLists(item);
                }

                await _actionHistoryService.InsertActionHistories(actionHistories);
                _context.ComplaintStructuralUnits.AddRange(citizenComplaintStructurals);
                await _context.CommitTransactionAsync();

                return Result.Success();

                // local function for audit action history
                void AddItemsToLists(int structuralUnitId)
                {
                    CitizenComplaintStructuralUnit entity = new CitizenComplaintStructuralUnit
                    {
                        CitizenComplaintId = request.CitizenComplaintId,
                        StructuralUnitId = structuralUnitId,
                        Status = CitizenComplaintStatus.InProgress
                    };
                    citizenComplaintStructurals.Add(entity);

                    var structuralUnitName = structuralUnits.Find(x => x.Id == structuralUnitId).Name;
                    ActionHistoryDTO actionHistoryDTO = new ActionHistoryDTO
                    {
                        ActionDate = _dateTime.Now,
                        Status = CitizenComplaintStatus.InProgress,
                        ComplaintId = citizenComplaint.Id,
                        Description = $"Заявка передана в {structuralUnitName}.",
                        UserTitle = structuralUnitName
                    };
                    actionHistories.Add(actionHistoryDTO);
                }
            }
            catch (Exception e)
            {
                await _context.RollbackTransactionAsync();
                _logger.LogError($"Complaint StructuralUnits creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}