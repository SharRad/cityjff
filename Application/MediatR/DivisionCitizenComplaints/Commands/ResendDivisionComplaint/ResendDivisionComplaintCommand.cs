﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.DivisionCitizenComplaints.Commands.ResendDivisionComplaint
{
    public class ResendDivisionComplaintCommand : IRequest<Result>
    {
        public int ComplaintId { get; set; }
        public int ActionHistoryId { get; set; }
        public int StructuralUnitId { get; set; }
    }

    public class ResendDivisionComplaintCommandHandler : IRequestHandler<ResendDivisionComplaintCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<ResendDivisionComplaintCommandHandler> _logger;
        private readonly IActionHistoryService _actionHistoryService;
        private readonly ICurrentUserService _userService;
        private readonly IDateTime _dateTime;

        public ResendDivisionComplaintCommandHandler
            (IApplicationDbContext context, ILogger<ResendDivisionComplaintCommandHandler> logger,
            IActionHistoryService actionHistoryService, IDateTime dateTime, ICurrentUserService userService)
        {
            _context = context;
            _logger = logger;
            _actionHistoryService = actionHistoryService;
            _userService = userService;
            _dateTime = dateTime;
        }

        public async Task<Result> Handle(ResendDivisionComplaintCommand request, CancellationToken cancellationToken)
        {
            var actionHistory = await _context.ActionHistories.FindAsync(request.ActionHistoryId);
            if (actionHistory == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            var structuralUnit = await _context.StructuralUnits.FindAsync(request.StructuralUnitId);
            if (structuralUnit == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            bool isExist = await _context.ComplaintStructuralUnits.AsNoTracking()
                .AnyAsync(x => x.CitizenComplaintId == actionHistory.ComplaintId && x.StructuralUnitId == request.StructuralUnitId);
            if (isExist)
                return Result.Failure(ApplicationResources.DataExist);

            try
            {
                await _context.BeginTransactionAsync();
                actionHistory.IsRedirected = true;
                CitizenComplaintStructuralUnit entity = new CitizenComplaintStructuralUnit
                {
                    CitizenComplaintId = actionHistory.ComplaintId,
                    StructuralUnitId = request.StructuralUnitId,
                    Status = CitizenComplaintStatus.InProgress,
                    Description = $"Заявка переназначена на {structuralUnit.Name}"
                };

                ActionHistoryDTO historyDTO = new ActionHistoryDTO
                {
                    ActionDate = _dateTime.Now,
                    Status = CitizenComplaintStatus.InProgress,
                    ComplaintId = actionHistory.ComplaintId,
                    Description = $"Заявка переназначена на {structuralUnit.Name}",
                    UserTitle = structuralUnit.Name
                };
                await _actionHistoryService.InsertActionHistory(historyDTO);

                _context.ActionHistories.Update(actionHistory);
                _context.ComplaintStructuralUnits.Add(entity);
                await _context.CommitTransactionAsync();

                return Result.Success();
            }
            catch (Exception e)
            {
                await _context.RollbackTransactionAsync();
                _logger.LogError($"Resend division complaint citizen creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}