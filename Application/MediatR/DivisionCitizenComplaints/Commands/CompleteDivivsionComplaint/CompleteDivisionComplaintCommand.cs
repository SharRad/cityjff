﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.DivisionCitizenComplaints.Commands.CompleteDivivsionComplaint
{
    public class CompleteDivisionComplaintCommand : IRequest<Result>
    {
        public int CitizenComplaintId { get; set; }
        public string Description { get; set; }
    }

    public class CompleteDivisionComplaintCommandHandler : IRequestHandler<CompleteDivisionComplaintCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CompleteDivisionComplaintCommandHandler> _logger;
        private readonly IActionHistoryService _actionHistoryService;
        private readonly ICurrentUserService _userService;
        private readonly IDateTime _dateTime;

        public CompleteDivisionComplaintCommandHandler
            (IApplicationDbContext context, ILogger<CompleteDivisionComplaintCommandHandler> logger,
            IActionHistoryService actionHistoryService, IDateTime dateTime, ICurrentUserService userService)
        {
            _context = context;
            _logger = logger;
            _actionHistoryService = actionHistoryService;
            _userService = userService;
            _dateTime = dateTime;
        }

        public async Task<Result> Handle(CompleteDivisionComplaintCommand request, CancellationToken cancellationToken)
        {
            var user = _userService.Get;

            var complaintStructuralUnits = await _context.ComplaintStructuralUnits
                .Where(x => x.CitizenComplaintId == request.CitizenComplaintId).ToListAsync(cancellationToken);

            var complaintStructuralUnit = complaintStructuralUnits.Find(x => x.StructuralUnitId == user.StructuralUnitId);
            if (complaintStructuralUnit == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            var structuralUnit = await _context.StructuralUnits.FindAsync(user.StructuralUnitId);

            try
            {
                await _context.BeginTransactionAsync();

                complaintStructuralUnit.Status = CitizenComplaintStatus.Completed;
                complaintStructuralUnit.Description = request.Description;

                _context.ComplaintStructuralUnits.Update(complaintStructuralUnit);
                await _context.SaveChangesAsync(cancellationToken);

                ActionHistoryDTO historyDTO = new ActionHistoryDTO
                {
                    ActionDate = _dateTime.Now,
                    Status = CitizenComplaintStatus.Completed,
                    ComplaintId = complaintStructuralUnit.CitizenComplaintId,
                    Description = $"{request.Description} - {structuralUnit.Name}",
                    UserTitle = structuralUnit.Name
                };

                if (complaintStructuralUnits.All(x => x.Status == CitizenComplaintStatus.Completed || x.Status == CitizenComplaintStatus.Canceled))
                {
                    var complaint = await _context.CitizenComplaints.FindAsync(request.CitizenComplaintId);
                    complaint.Status = CitizenComplaintStatus.ReadyToComplete;
                    _context.CitizenComplaints.Update(complaint);

                    ActionHistoryDTO mainhistoryDTO = new ActionHistoryDTO
                    {
                        ActionDate = _dateTime.Now,
                        Status = CitizenComplaintStatus.ReadyToComplete,
                        ComplaintId = complaint.Id,
                        Description = "Заявка готова к завершению Мэрией",
                        UserTitle = "Мэрия Бишкек"
                    };
                    await _actionHistoryService.InsertActionHistory(mainhistoryDTO);
                }

                await _actionHistoryService.InsertActionHistory(historyDTO);
                await _context.CommitTransactionAsync();

                return Result.Success();
            }
            catch (Exception e)
            {
                await _context.RollbackTransactionAsync();
                _logger.LogError($"Edit division complaint citizen creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}