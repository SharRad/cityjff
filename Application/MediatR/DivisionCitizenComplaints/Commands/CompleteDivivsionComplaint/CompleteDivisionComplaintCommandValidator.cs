﻿using FluentValidation;

namespace Application.MediatR.DivisionCitizenComplaints.Commands.CompleteDivivsionComplaint
{
    public class CompleteDivisionComplaintCommandValidator : AbstractValidator<CompleteDivisionComplaintCommand>
    {
        public CompleteDivisionComplaintCommandValidator()
        {
            RuleFor(v => v.Description)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}