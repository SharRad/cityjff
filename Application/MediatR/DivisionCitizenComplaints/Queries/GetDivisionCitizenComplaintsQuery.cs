﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.DivisionCitizenComplaints.Queries
{
    public class GetDivisionCitizenComplaintsQuery : IRequest<IPager<CitizenComplaintDto>>
    {
        public int Page { get; set; }
    }

    public class GetDivisionCitizenComplaintsQueryHandler : IRequestHandler<GetDivisionCitizenComplaintsQuery, IPager<CitizenComplaintDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly ICurrentUserService _userService;

        public GetDivisionCitizenComplaintsQueryHandler(IApplicationDbContext context, ICurrentUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        public Task<IPager<CitizenComplaintDto>> Handle(GetDivisionCitizenComplaintsQuery request, CancellationToken cancellationToken)
        {
            var user = _userService.Get;
            return _context.ComplaintStructuralUnits
                .AsNoTracking()
                .Where(x => x.StructuralUnitId == user.StructuralUnitId)
                .Select(x => new CitizenComplaintDto
                {
                    Id = x.CitizenComplaintId,
                    Status = x.Status,
                    Address = x.CitizenComplaint.Address,
                    Description = x.CitizenComplaint.Description
                })
                .OrderBy(x => x.Status)
                .ToPagerListAsync(request.Page, 10, cancellationToken);
        }
    }
}