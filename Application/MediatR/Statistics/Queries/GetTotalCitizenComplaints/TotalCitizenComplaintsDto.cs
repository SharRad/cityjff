﻿namespace Application.MediatR.Statistics.Queries.GetTotalCitizenComplaints
{
    public class TotalCitizenComplaintsDto
    {
        public int TotalCount { get; set; }
        public int MonthCount { get; set; }
        public double MonthlyGrowthProcent { get; set; }
    }
}