﻿using Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Statistics.Queries.GetTotalCitizenComplaints
{
    public class GetTotalCitizenComplaintsQuery : IRequest<TotalCitizenComplaintsDto>
    {
    }

    public class GetTotalCitizenComplaintsQueryHandler : IRequestHandler<GetTotalCitizenComplaintsQuery, TotalCitizenComplaintsDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IDateTime _dateTime;

        public GetTotalCitizenComplaintsQueryHandler(IApplicationDbContext context, IDateTime dateTime)
        {
            _context = context;
            _dateTime = dateTime;
        }

        public async Task<TotalCitizenComplaintsDto> Handle(GetTotalCitizenComplaintsQuery request, CancellationToken cancellationToken)
        {
            int totalCount = await _context.CitizenComplaints.CountAsync(cancellationToken);
            int monthCount = await _context.CitizenComplaints
                .CountAsync(c => c.Created.Month == _dateTime.Now.Month, cancellationToken);

            double monthlyGrows = CalculateMonthlyGrowthInPercent();

            return new TotalCitizenComplaintsDto
            {
                TotalCount = totalCount,
                MonthCount = monthCount,
                MonthlyGrowthProcent = monthlyGrows
            };

            double CalculateMonthlyGrowthInPercent()
            {
                return totalCount != 0 ? ((double)monthCount / (double)totalCount) * 100 : 0.0;
            }
        }
    }
}