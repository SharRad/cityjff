﻿using Application.Common.Extensions;
using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Statistics.Queries.GetCitizenComplaintsByMonthly
{
    public class GetCitizenComplaintsByMonthlyQuery : IRequest<MonthlyCitizenComplaintsDto>
    {
    }

    public class GetCitizenComplaintsByMonthlyQueryHandler : IRequestHandler<GetCitizenComplaintsByMonthlyQuery, MonthlyCitizenComplaintsDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IDateTime _dateTime;

        public GetCitizenComplaintsByMonthlyQueryHandler(IApplicationDbContext context, IDateTime dateTime)
        {
            _context = context;
            _dateTime = dateTime;
        }

        public async Task<MonthlyCitizenComplaintsDto> Handle(GetCitizenComplaintsByMonthlyQuery request, CancellationToken cancellationToken)
        {
            int currentMonth = _dateTime.Now.Month;

            var monthsTotalCountGroups = await _context.CitizenComplaints
                .Where(c => c.Created.Month <= currentMonth && c.Created.AddMonths(-7).Month >= currentMonth)
                .GroupBy(c => c.Created.Month)
                .Select(c => new { Month = c.Key, Count = c.Count() })
                .ToListAsync();

            var monthsCompletedCountGroups = await _context.CitizenComplaints
                .Where(c => c.Created.Month <= currentMonth && c.Created.AddMonths(-7).Month >= currentMonth && c.Status == CitizenComplaintStatus.Completed)
                .GroupBy(c => c.Created.Month)
                .Select(c => new { Month = c.Key, Count = c.Count() })
                .ToListAsync();

            var monthNames = new Stack<string>();
            var totalMonthsCount = new Stack<int>();
            var completedMonthsCount = new Stack<int>();

            for (int i = 0; i < 7; i++)
            {
                monthNames.Push(currentMonth.GetRuMonthName());

                var monthsTotalGroup = monthsTotalCountGroups.SingleOrDefault(c => c.Month == currentMonth);
                totalMonthsCount.Push(monthsTotalGroup?.Count ?? 0);

                var monthsCompletedGroup = monthsCompletedCountGroups.SingleOrDefault(c => c.Month == currentMonth);
                completedMonthsCount.Push(monthsCompletedGroup?.Count ?? 0);

                if (--currentMonth == 0)
                    currentMonth = 12;
            }

            return new MonthlyCitizenComplaintsDto
            {
                Monthes = monthNames,
                MonthsTotalCount = totalMonthsCount,
                MonthsCompletedCount = completedMonthsCount
            };
        }
    }
}