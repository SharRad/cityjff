﻿using System.Collections.Generic;

namespace Application.MediatR.Statistics.Queries.GetCitizenComplaintsByMonthly
{
    public class MonthlyCitizenComplaintsDto
    {
        public IEnumerable<string> Monthes { get; set; }
        public IEnumerable<int> MonthsTotalCount { get; set; }
        public IEnumerable<int> MonthsCompletedCount { get; set; }
    }
}