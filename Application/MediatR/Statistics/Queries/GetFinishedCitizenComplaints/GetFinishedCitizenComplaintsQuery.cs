﻿using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaints
{
    public class GetFinishedCitizenComplaintsQuery : IRequest<FinishedCitizenComplaintsDto>
    {
    }

    public class GetFinishedCitizenComplaintsQueryHandler : IRequestHandler<GetFinishedCitizenComplaintsQuery, FinishedCitizenComplaintsDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IDateTime _dateTime;

        public GetFinishedCitizenComplaintsQueryHandler(IApplicationDbContext context, IDateTime dateTime)
        {
            _context = context;
            _dateTime = dateTime;
        }

        public async Task<FinishedCitizenComplaintsDto> Handle(GetFinishedCitizenComplaintsQuery request, CancellationToken cancellationToken)
        {
            int finishedCount = await _context.CitizenComplaints
                .CountAsync(c => c.Status == CitizenComplaintStatus.Completed, cancellationToken);

            int finishedMonthCount = await _context.CitizenComplaints
                .CountAsync(c => c.Status == CitizenComplaintStatus.Completed && c.Created.Month == _dateTime.Now.Month, cancellationToken);

            double monthlyGrows = CalculateMonthlyGrowthInPercent();

            return new FinishedCitizenComplaintsDto
            {
                FinishedCount = finishedCount,
                MonthCount = finishedMonthCount,
                MonthlyGrowthProcent = monthlyGrows
            };

            double CalculateMonthlyGrowthInPercent()
            {
                return finishedCount != 0 ? ((double)finishedMonthCount / (double)finishedCount) * 100 : 0.0;
            }
        }
    }
}