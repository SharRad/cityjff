﻿namespace Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaints
{
    public class FinishedCitizenComplaintsDto
    {
        public int FinishedCount { get; set; }
        public int MonthCount { get; set; }
        public double MonthlyGrowthProcent { get; set; }
    }
}