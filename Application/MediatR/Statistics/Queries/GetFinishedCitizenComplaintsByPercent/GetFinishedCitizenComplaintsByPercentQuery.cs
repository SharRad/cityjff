﻿using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaintsByPercent
{
    public class GetFinishedCitizenComplaintsByPercentQuery : IRequest<double>
    {
    }

    public class GetFinishedCitizenComplaintsByPercentQueryHandler : IRequestHandler<GetFinishedCitizenComplaintsByPercentQuery, double>
    {
        private readonly IApplicationDbContext _context;

        public GetFinishedCitizenComplaintsByPercentQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<double> Handle(GetFinishedCitizenComplaintsByPercentQuery request, CancellationToken cancellationToken)
        {
            double totalCount = await _context.CitizenComplaints.CountAsync(cancellationToken);
            double finishedCount = await _context.CitizenComplaints.CountAsync(c => c.Status == CitizenComplaintStatus.Completed, cancellationToken);

            return totalCount != 0 ? (finishedCount / totalCount) * 100 : 0.0;
        }
    }
}