﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.StructuralUnits.Queries.GetStructuralUnits
{
    public class GetStructuralUnitByIdQuery : IRequest<StructuralUnitDto>
    {
        public int Id { get; set; }
    }

    public class GetStructuralUnitByIdQueryHandler : IRequestHandler<GetStructuralUnitByIdQuery, StructuralUnitDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetStructuralUnitByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<StructuralUnitDto> Handle(GetStructuralUnitByIdQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<StructuralUnitDto>(_context.StructuralUnits.AsNoTracking())
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        }
    }
}