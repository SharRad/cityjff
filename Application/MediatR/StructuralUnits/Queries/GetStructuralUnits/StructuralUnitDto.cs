﻿using Application.Common.Mappings;
using Domain.Entities;

namespace Application.MediatR.StructuralUnits.Queries.GetStructuralUnits
{
    public class StructuralUnitDto : IMapFrom<StructuralUnit>
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}