﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.StructuralUnits.Queries.GetStructuralUnits
{
    public class GetStructuralUnitsQuery : IRequest<List<StructuralUnitDto>>
    {
    }

    public class GetStructuralUnitsQueryHandler : IRequestHandler<GetStructuralUnitsQuery, List<StructuralUnitDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetStructuralUnitsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<List<StructuralUnitDto>> Handle(GetStructuralUnitsQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<StructuralUnitDto>(_context.StructuralUnits.AsNoTracking())
                .ToListAsync(cancellationToken);
        }
    }
}