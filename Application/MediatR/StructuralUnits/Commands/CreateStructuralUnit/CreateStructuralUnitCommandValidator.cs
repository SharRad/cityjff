﻿using FluentValidation;

namespace Application.MediatR.StructuralUnits.Commands.CreateStructuralUnit
{
    public class CreateStructuralUnitCommandValidator : AbstractValidator<CreateStructuralUnitCommand>
    {
        public CreateStructuralUnitCommandValidator()
        {
            RuleFor(v => v.Name)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}