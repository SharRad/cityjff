﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.StructuralUnits.Commands.CreateStructuralUnit
{
    public class CreateStructuralUnitCommand : IRequest<Result>
    {
        public string Name { get; set; }
    }

    public class CreateStructuralUnitCommandHandler : IRequestHandler<CreateStructuralUnitCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CreateStructuralUnitCommandHandler> _logger;

        public CreateStructuralUnitCommandHandler(IApplicationDbContext context, ILogger<CreateStructuralUnitCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(CreateStructuralUnitCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var mayorOffice = await _context.MayorOffices.FirstOrDefaultAsync();
                _context.StructuralUnits.Add(new StructuralUnit
                {
                    Name = request.Name,
                    MayorOfficeId = mayorOffice.Id
                });

                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"Structural Unit creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}