﻿using FluentValidation;

namespace Application.MediatR.StructuralUnits.Commands.EditStructuralUnit
{
    public class EditStructuralUnitCommandValidation : AbstractValidator<EditStructuralUnitCommand>
    {
        public EditStructuralUnitCommandValidation()
        {
            RuleFor(v => v.Id).NotEmpty();
            RuleFor(v => v.Name)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}