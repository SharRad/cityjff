﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.StructuralUnits.Commands.EditStructuralUnit
{
    public class EditStructuralUnitCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class EditStructuralUnitCommandHandler : IRequestHandler<EditStructuralUnitCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<EditStructuralUnitCommandHandler> _logger;

        public EditStructuralUnitCommandHandler(IApplicationDbContext context, ILogger<EditStructuralUnitCommandHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Result> Handle(EditStructuralUnitCommand request, CancellationToken cancellationToken)
        {
            var structuralUnit = await _context.StructuralUnits.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (structuralUnit == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                structuralUnit.Name = request.Name;

                _context.StructuralUnits.Update(structuralUnit);
                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (Exception e)
            {
                _logger.LogError($"Structural Unit edit failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.EditError);
            }
        }
    }
}