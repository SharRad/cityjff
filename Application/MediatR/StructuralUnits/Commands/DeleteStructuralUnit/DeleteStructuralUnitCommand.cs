﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.StructuralUnits.Commands.DeleteStructuralUnit
{
    public class DeleteStructuralUnitCommand : IRequest<Result>
    {
        public int Id { get; set; }
    }

    public class DeleteStructuralUnitCommandHandler : IRequestHandler<DeleteStructuralUnitCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<DeleteStructuralUnitCommandHandler> _logger;
        private readonly ICustomExceptionHandler _exceptionHandler;

        public DeleteStructuralUnitCommandHandler(ILogger<DeleteStructuralUnitCommandHandler> logger, IApplicationDbContext context, ICustomExceptionHandler exceptionHandler)
        {
            _logger = logger;
            _context = context;
            _exceptionHandler = exceptionHandler;
        }

        public async Task<Result> Handle(DeleteStructuralUnitCommand request, CancellationToken cancellationToken)
        {
            var structuralUnit = await _context.StructuralUnits.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (structuralUnit == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                _context.StructuralUnits.Remove(structuralUnit);

                await _context.SaveChangesAsync(cancellationToken);

                return Result.Success();
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError($"Structural Unit remove failed with error: {ex.InnerException.HResult}");
                return Result.Failure(_exceptionHandler.CatchDbException(ex));
            }
            catch (Exception e)
            {
                _logger.LogError($"Structural Unit remove failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.DeleteError);
            }
        }
    }
}