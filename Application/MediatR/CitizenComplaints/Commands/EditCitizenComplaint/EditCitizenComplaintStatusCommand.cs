﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Commands.EditCitizenComplaint
{
    public class EditCitizenComplaintStatusCommand : IRequest<Result>
    {
        public int CitizenComplaintId { get; set; }
        public CitizenComplaintStatus Status { get; set; }
        public string Description { get; set; }
    }

    public class EditCitizenComplaintStatusCommandHandler : IRequestHandler<EditCitizenComplaintStatusCommand, Result>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<EditCitizenComplaintStatusCommandHandler> _logger;
        private readonly IActionHistoryService _actionHistoryService;
        private readonly IDateTime _dateTime;

        public EditCitizenComplaintStatusCommandHandler
            (IApplicationDbContext context, ILogger<EditCitizenComplaintStatusCommandHandler> logger,
            IActionHistoryService actionHistoryService, IDateTime dateTime)
        {
            _context = context;
            _logger = logger;
            _actionHistoryService = actionHistoryService;
            _dateTime = dateTime;
        }

        public async Task<Result> Handle(EditCitizenComplaintStatusCommand request, CancellationToken cancellationToken)
        {
            var citizenComplaint = await _context.CitizenComplaints.SingleOrDefaultAsync(x => x.Id == request.CitizenComplaintId, cancellationToken);
            if (citizenComplaint == null)
                return Result.Failure(ApplicationResources.RecordNotFoundError);

            try
            {
                citizenComplaint.Status = request.Status;
                ActionHistoryDTO historyDTO = new ActionHistoryDTO
                {
                    ActionDate = _dateTime.Now,
                    Status = request.Status,
                    ComplaintId = citizenComplaint.Id,
                    Description = request.Description,
                    UserTitle = "Мэрия Бишкек"
                };
                await _context.BeginTransactionAsync();
                await _actionHistoryService.InsertActionHistory(historyDTO);
                _context.CitizenComplaints.Update(citizenComplaint);
                await _context.CommitTransactionAsync();

                return Result.Success();
            }
            catch (Exception e)
            {
                await _context.RollbackTransactionAsync();
                _logger.LogError($"Edit complaint citizen creation failed with error: {e.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}