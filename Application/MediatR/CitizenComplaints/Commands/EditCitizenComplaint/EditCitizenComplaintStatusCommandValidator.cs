﻿using FluentValidation;

namespace Application.MediatR.CitizenComplaints.Commands.EditCitizenComplaint
{
    public class EditCitizenComplaintStatusCommandValidator : AbstractValidator<EditCitizenComplaintStatusCommand>
    {
        public EditCitizenComplaintStatusCommandValidator()
        {
            RuleFor(v => v.Status)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.Description)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}