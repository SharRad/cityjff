﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Commands.CreateCitizenComplaint
{
    public class CreateCitizenComplaintCommand : IRequest<(Result, int)>
    {
        public string Description { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public IFormFileCollection Images { get; set; }
    }

    public class CreateCitizenComplaintCommandHandler : IRequestHandler<CreateCitizenComplaintCommand, (Result, int)>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CreateCitizenComplaintCommandHandler> _logger;
        private readonly IFileService _fileService;
        private readonly IActionHistoryService _actionHistoryService;
        private readonly ICurrentUserService _userService;

        public CreateCitizenComplaintCommandHandler(IApplicationDbContext context, IFileService fileService,
            ILogger<CreateCitizenComplaintCommandHandler> logger, IActionHistoryService actionHistoryService, ICurrentUserService userService)
        {
            _context = context;
            _logger = logger;
            _fileService = fileService;
            _actionHistoryService = actionHistoryService;
            _userService = userService;
        }

        public async Task<(Result, int)> Handle(CreateCitizenComplaintCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string folderFileName = _fileService.AddGeoFile(request.Latitude, request.Longitude);
                var citizenComplaint = new CitizenComplaint
                {
                    Description = request.Description,
                    Address = request.Address,
                    Latitude = request.Latitude,
                    Longitude = request.Longitude,
                    MayorOfficeId = 1,
                    Status = CitizenComplaintStatus.New,
                    GeolocationImagePath = folderFileName,
                    TokenFirebaseId = 1
                };

                await _context.BeginTransactionAsync();
                await LoadImages();

                _context.CitizenComplaints.Add(citizenComplaint);
                await _context.SaveChangesAsync(cancellationToken);

                await SaveHistory();
                await _context.CommitTransactionAsync();

                return (Result.Success(), citizenComplaint.Id);

                // local function
                async Task LoadImages()
                {
                    var pathes = await _fileService.AddFilesAsync(request.Images);

                    if (pathes?.Any() == true)
                    {
                        foreach (var path in pathes)
                        {
                            citizenComplaint.CitizenComplaintImages.Add(new CitizenComplaintImage
                            {
                                FileUrl = path
                            });
                        }
                    }
                }

                // local function for audit action history
                async Task SaveHistory()
                {
                    string userName = _userService?.Get?.UserName;
                    ActionHistoryDTO actionHistoryDTO = new ActionHistoryDTO
                    {
                        ActionDate = DateTime.Now,
                        Status = CitizenComplaintStatus.New,
                        ComplaintId = citizenComplaint.Id,
                        Description = $"Cоздана новая заявка {userName ?? "Анонимно"}.",
                        UserTitle = userName ?? "Анонимно"
                    };
                    await _actionHistoryService.InsertActionHistory(actionHistoryDTO);
                }
            }
            catch (Exception e)
            {
                await _context.RollbackTransactionAsync();
                _logger.LogError($"Citizen complaint creation failed with error: {e.Message}");
                return (Result.Failure(ApplicationResources.CreationError), -1);
            }
        }
    }
}