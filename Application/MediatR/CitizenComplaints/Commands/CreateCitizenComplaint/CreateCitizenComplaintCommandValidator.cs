﻿using FluentValidation;

namespace Application.MediatR.CitizenComplaints.Commands.CreateCitizenComplaint
{
    public class CreateCitizenComplaintCommandValidator : AbstractValidator<CreateCitizenComplaintCommand>
    {
        public CreateCitizenComplaintCommandValidator()
        {
            RuleFor(v => v.Address)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
            RuleFor(v => v.Description)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
            RuleFor(v => v.Latitude)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.Longitude)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}