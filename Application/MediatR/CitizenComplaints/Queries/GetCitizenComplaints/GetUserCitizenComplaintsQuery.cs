﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints
{
    public class GetUserCitizenComplaintsQuery : IRequest<List<CitizenComplaintShortDto>>
    {
    }

    public class GetUserCitizenComplaintsQueryHandler : IRequestHandler<GetUserCitizenComplaintsQuery, List<CitizenComplaintShortDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetUserCitizenComplaintsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<List<CitizenComplaintShortDto>> Handle(GetUserCitizenComplaintsQuery request, CancellationToken cancellationToken)
        {
            int currentUserId = 2;

            return _context.CitizenComplaints
                .Where(c => c.CreatedBy == currentUserId)
                .AsNoTracking()
                .ProjectTo<CitizenComplaintShortDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}