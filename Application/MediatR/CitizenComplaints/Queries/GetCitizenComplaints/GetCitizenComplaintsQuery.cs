﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using P.Pager;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints
{
    public class GetCitizenComplaintsQuery : IRequest<IPager<CitizenComplaintDto>>
    {
        public int Page { get; set; }
    }

    public class GetCitizenComplaintsQueryHandler : IRequestHandler<GetCitizenComplaintsQuery, IPager<CitizenComplaintDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCitizenComplaintsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<IPager<CitizenComplaintDto>> Handle(GetCitizenComplaintsQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<CitizenComplaintDto>(_context.CitizenComplaints.AsNoTracking().Where(c => c.MayorOfficeId == 1).OrderBy(x => x.Status))
                .ToPagerListAsync(request.Page, 20, cancellationToken);
        }
    }
}