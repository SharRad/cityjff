﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints
{
    public class GetCitizenComplaintsWithoutNewQuery : IRequest<List<CitizenComplaintShortDto>>
    {
    }

    public class GetCitizenComplaintsWithoutNewQueryHandler : IRequestHandler<GetCitizenComplaintsWithoutNewQuery, List<CitizenComplaintShortDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCitizenComplaintsWithoutNewQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<List<CitizenComplaintShortDto>> Handle(GetCitizenComplaintsWithoutNewQuery request, CancellationToken cancellationToken)
        {
            return _context.CitizenComplaints
                .Where(c => c.Status != CitizenComplaintStatus.New)
                .AsNoTracking()
                .ProjectTo<CitizenComplaintShortDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}