﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints
{
    public class GetCitizenComplaintCountQuery : IRequest<int>
    {
    }

    public class GetCitizenComplaintCountQueryHandler : IRequestHandler<GetCitizenComplaintCountQuery, int>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCitizenComplaintCountQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public Task<int> Handle(GetCitizenComplaintCountQuery request, CancellationToken cancellationToken)
        {
            return _mapper.ProjectTo<CitizenComplaintDto>(_context.CitizenComplaints.Where(x => x.MayorOfficeId == 1).AsNoTracking())
                .CountAsync(cancellationToken);
        }
    }
}