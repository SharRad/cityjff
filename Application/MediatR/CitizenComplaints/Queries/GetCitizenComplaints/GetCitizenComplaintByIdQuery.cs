﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints
{
    public class GetCitizenComplaintByIdQuery : IRequest<CitizenComplaintDto>
    {
        public int Id { get; set; }
    }

    public class GetCitizenComplaintByIdQueryHandler : IRequestHandler<GetCitizenComplaintByIdQuery, CitizenComplaintDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IIdentityService _identityService;

        public GetCitizenComplaintByIdQueryHandler(IApplicationDbContext context, IMapper mapper, IIdentityService identityService)
        {
            _context = context;
            _mapper = mapper;
            _identityService = identityService;
        }

        public async Task<CitizenComplaintDto> Handle(GetCitizenComplaintByIdQuery request, CancellationToken cancellationToken)
        {
            var citizenComplaints = await _context.CitizenComplaints
                .Include(c => c.CitizenComplaintImages)
                .Include(ah => ah.CitizenComplaintStructuralUnits)
                    .ThenInclude(x => x.StructuralUnit)
                .Include(ah => ah.ActionHistories)
                .AsNoTracking()
                .SingleOrDefaultAsync(c => c.Id == request.Id, cancellationToken);

            var citizenComplaintsDto = _mapper.Map<CitizenComplaintDto>(citizenComplaints);

            if (citizenComplaints.CreatedBy != 0)
            {
                var user = await _identityService.GetUserByIdAsync(citizenComplaints.CreatedBy);

                citizenComplaintsDto.FirstName = user.FirstName;
                citizenComplaintsDto.LastName = user.LastName;
                citizenComplaintsDto.UserEmail = user.UserName;
            }

            return citizenComplaintsDto;
        }
    }
}