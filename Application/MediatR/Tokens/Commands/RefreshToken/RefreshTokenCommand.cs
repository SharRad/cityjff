﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Tokens.Commands.RefreshToken
{
    public class RefreshTokenCommand : IRequest<(Result, ApiUserToken)>
    {
        public string RefreshToken { get; set; }
    }

    public class RefreshTokenCommandHandler : IRequestHandler<RefreshTokenCommand, (Result, ApiUserToken)>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<RefreshTokenCommandHandler> _logger;

        public RefreshTokenCommandHandler(IIdentityService identityService, ILogger<RefreshTokenCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<(Result, ApiUserToken)> Handle(RefreshTokenCommand command, CancellationToken cancellationToken)
        {
            try
            {
                UserRefreshToken userRefreshData = new UserRefreshToken
                {
                    RefreshToken = command.RefreshToken
                };
                return await _identityService.RefreshToken(userRefreshData);
            }
            catch (Exception e)
            {
                _logger.LogError($"User authenticate failed with error: {e.Message}");
                return (Result.Failure(ApplicationResources.CreationError), null);
            }
        }
    }
}