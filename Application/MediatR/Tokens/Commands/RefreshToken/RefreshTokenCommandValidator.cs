﻿using FluentValidation;

namespace Application.MediatR.Tokens.Commands.RefreshToken
{
    public class RefreshTokenCommandValidator : AbstractValidator<RefreshTokenCommand>
    {
        public RefreshTokenCommandValidator()
        {
            RuleFor(v => v.RefreshToken)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}