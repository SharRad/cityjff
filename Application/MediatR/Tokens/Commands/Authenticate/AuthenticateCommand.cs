﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Tokens.Commands.Authenticate
{
    public class AuthenticateCommand : IRequest<(Result, ApiUserToken)>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirebaseToken { get; set; }
    }

    public class AuthenticateCommandHandler : IRequestHandler<AuthenticateCommand, (Result, ApiUserToken)>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<AuthenticateCommandHandler> _logger;

        public AuthenticateCommandHandler(IIdentityService identityService, ILogger<AuthenticateCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<(Result, ApiUserToken)> Handle(AuthenticateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var authdata = new UserAuthData
                {
                    Username = command.Username,
                    Password = command.Password,
                    FirebaseToken = command.FirebaseToken
                };
                return await _identityService.Authenticate(authdata);
            }
            catch (Exception e)
            {
                _logger.LogError($"User authenticate failed with error: {e.Message}");
                return (Result.Failure(ApplicationResources.CreationError), null);
            }
        }
    }
}