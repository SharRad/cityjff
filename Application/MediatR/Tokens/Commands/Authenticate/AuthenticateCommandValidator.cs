﻿using FluentValidation;

namespace Application.MediatR.Tokens.Commands.Authenticate
{
    public class AuthenticateCommandValidator : AbstractValidator<AuthenticateCommand>
    {
        public AuthenticateCommandValidator()
        {
            RuleFor(v => v.Username)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
            RuleFor(v => v.Password)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
            RuleFor(v => v.FirebaseToken)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .MaximumLength(200).WithMessage(ApplicationResources.MaxLengthValidation);
        }
    }
}