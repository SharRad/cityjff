﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Accounts.Commands.BanAccount
{
    public class BanAccountCommand : IRequest<Result>
    {
        public int Id { get; set; }
    }

    public class BanAccountCommandHandler : IRequestHandler<BanAccountCommand, Result>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<BanAccountCommandHandler> _logger;

        public BanAccountCommandHandler(IIdentityService identityService, ILogger<BanAccountCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<Result> Handle(BanAccountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                return await _identityService.BanAccount(request.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{GetType().Name} failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}