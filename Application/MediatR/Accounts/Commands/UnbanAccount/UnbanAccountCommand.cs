﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Accounts.Commands.UnbanAccount
{
    public class UnUnbanAccountCommand
    {
    }

    public class UnbanAccountCommand : IRequest<Result>
    {
        public int Id { get; set; }
    }

    public class UnbanAccountCommandHandler : IRequestHandler<UnbanAccountCommand, Result>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<UnbanAccountCommandHandler> _logger;

        public UnbanAccountCommandHandler(IIdentityService identityService, ILogger<UnbanAccountCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<Result> Handle(UnbanAccountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                return await _identityService.UnBanAccount(request.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{GetType().Name} failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}