﻿using FluentValidation;

namespace Application.MediatR.Accounts.Commands.Login
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(c => c.Email).EmailAddress()
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.Password).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}