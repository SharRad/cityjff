﻿using FluentValidation;

namespace Application.MediatR.Accounts.Commands.SignOut
{
    public class SignOutCommandValidator : AbstractValidator<SignOutCommand>
    {
        public SignOutCommandValidator()
        {
            RuleFor(c => c.UserId).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}