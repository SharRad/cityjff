﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Accounts.Commands.SignOut
{
    public class SignOutCommand : IRequest<Result>
    {
        public int UserId { get; set; }
    }

    public class SignOutCommandHandler : IRequestHandler<SignOutCommand, Result>
    {
        private readonly IIdentityService _identityService;

        public SignOutCommandHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<Result> Handle(SignOutCommand command, CancellationToken cancellationToken)
        {
            return await _identityService.ApiSignOutAsync(command.UserId);
        }
    }
}