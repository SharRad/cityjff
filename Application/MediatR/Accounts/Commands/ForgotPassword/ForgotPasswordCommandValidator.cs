﻿using FluentValidation;

namespace Application.MediatR.Accounts.Commands.ForgotPassword
{
    public class ForgotPasswordCommandValidator : AbstractValidator<ForgotPasswordCommand>
    {
        public ForgotPasswordCommandValidator()
        {
            RuleFor(c => c.Email)
                .EmailAddress()
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.Password).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.Code).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}