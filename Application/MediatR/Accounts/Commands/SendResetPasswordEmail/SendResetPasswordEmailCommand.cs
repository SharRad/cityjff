﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Accounts.Commands.SendResetPasswordEmail
{
    public class SendResetPasswordEmailCommand : IRequest<Result>
    {
        /// <summary>
        /// Почтовый адрес
        /// </summary>
        public string Email { get; set; }
    }

    /// <summary>
    /// обработчик команды
    /// </summary>
    public class SendResetPasswordEmailCommandHandler : IRequestHandler<SendResetPasswordEmailCommand, Result>
    {
        private readonly IEmailSenderService _emailSenderService;
        private readonly ILogger<SendResetPasswordEmailCommandHandler> _logger;

        public SendResetPasswordEmailCommandHandler(IEmailSenderService emailSenderService, ILogger<SendResetPasswordEmailCommandHandler> logger)
        {
            _emailSenderService = emailSenderService;
            _logger = logger;
        }

        public async Task<Result> Handle(SendResetPasswordEmailCommand command, CancellationToken cancellationToken)
        {
            string callbackUrl = "https://localhost:44311/Account/ResetPassword";

            EmailMessage emailMessage = new EmailMessage(command.Email, "Password reset",
               $"You can reset your password by following this link: <a href='{callbackUrl}'>link</a>");

            try
            {
                await _emailSenderService.SendEmailAsync(emailMessage);

                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Send email operation failed with error: {ex.Message}");
                return Result.Failure("Send email operation failed");
            }
        }
    }
}