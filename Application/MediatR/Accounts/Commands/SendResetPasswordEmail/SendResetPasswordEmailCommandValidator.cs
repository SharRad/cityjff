﻿using FluentValidation;

namespace Application.MediatR.Accounts.Commands.SendResetPasswordEmail
{
    public class SendResetPasswordEmailCommandValidator : AbstractValidator<SendResetPasswordEmailCommand>
    {
        public SendResetPasswordEmailCommandValidator()
        {
            RuleFor(c => c.Email).EmailAddress().NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}