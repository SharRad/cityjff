﻿using FluentValidation;

namespace Application.MediatR.Accounts.Commands.ChangePassword
{
    public class ChangePasswordCommandValidator : AbstractValidator<ChangePasswordCommand>
    {
        public ChangePasswordCommandValidator()
        {
            RuleFor(v => v.UserId)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.OldPassword)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.Password)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(v => v.ConfirmPassword)
                .NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation)
                .Equal(x => x.Password).WithMessage(ApplicationResources.PasswordConfirm); ;
        }
    }
}