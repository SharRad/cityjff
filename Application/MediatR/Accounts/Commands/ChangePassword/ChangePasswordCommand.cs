﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Accounts.Commands.ChangePassword
{
    public class ChangePasswordCommand : IRequest<Result>
    {
        public int UserId { get; set; }

        /// <summary>
        /// Старый пароль
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// Новый пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Повторите пароль
        /// </summary>
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordCommandHandler : IRequestHandler<ChangePasswordCommand, Result>
    {
        private readonly IIdentityService _identityService;

        public ChangePasswordCommandHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<Result> Handle(ChangePasswordCommand command, CancellationToken cancellationToken)
        {
            ChangePasswordDTO dto = new ChangePasswordDTO
            {
                UserId = command.UserId,
                OldPassword = command.OldPassword,
                Password = command.Password,
                ConfirmPassword = command.ConfirmPassword
            };

            return await _identityService.ChangePassword(dto);
        }
    }
}