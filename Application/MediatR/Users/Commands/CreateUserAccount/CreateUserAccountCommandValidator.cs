﻿using Application.Common.Interfaces;
using Application.Common.Validators;
using FluentValidation;

namespace Application.MediatR.Users.Commands.CreateUserAccount
{
    public class CreateUserAccountCommandValidator : AbstractValidator<CreateUserAccountCommand>
    {
        public CreateUserAccountCommandValidator(IIdentityService service)
        {
            RuleFor(v => v.Email).MaximumLength(50).NotEmpty().EmailAddress().SetValidator(new UniqueEmailValidator(service));
            RuleFor(c => c.UserName).MaximumLength(50).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.FirstName).MaximumLength(50).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.LastName).MaximumLength(50).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.DateOfBirth).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.Gender).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.Password).MinimumLength(8).NotEmpty().WithMessage(ApplicationResources.FieldRequiredValidation);
            RuleFor(c => c.ConfirmPassword).MinimumLength(8).NotEmpty().Equal(x => x.Password).WithMessage(ApplicationResources.FieldRequiredValidation);
        }
    }
}