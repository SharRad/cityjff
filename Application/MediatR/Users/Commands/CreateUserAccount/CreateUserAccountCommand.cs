﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Identity;
using Domain.Enums;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Users.Commands.CreateUserAccount
{
    public class CreateUserAccountCommand : IRequest<Result>
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class CreateUserAccountCommandHandler : IRequestHandler<CreateUserAccountCommand, Result>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<CreateUserAccountCommandHandler> _logger;

        public CreateUserAccountCommandHandler(IIdentityService identityService, ILogger<CreateUserAccountCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<Result> Handle(CreateUserAccountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = new ApplicationUser
                {
                    UserName = request.UserName,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    DateOfBirth = request.DateOfBirth,
                    Gender = request.Gender,
                    Email = request.Email,
                    UserStatus = UserStatus.Active,
                    IsRoot = false
                };

                var (Result, userId) = await _identityService.CreateUserAsync(user, request.Password, AccessControlLevel.User);

                if (!Result.Succeeded)
                {
                    var exception = new Exception(Result.Errors.ToString());
                    throw exception;
                }
                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{GetType().Name} failed with error: {ex.Message}");

                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}