﻿using Application.Common.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Users.Queries.GetUsers
{
    public class GetMayorOfficeUsersIdQuery : IRequest<List<string>>
    {
        public int MayorOfficeId { get; set; }
    }

    public class GetMayorOfficeUsersIdQueryHandler : IRequestHandler<GetMayorOfficeUsersIdQuery, List<string>>
    {
        private readonly IIdentityService _identityService;

        public GetMayorOfficeUsersIdQueryHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public Task<List<string>> Handle(GetMayorOfficeUsersIdQuery request, CancellationToken cancellationToken)
        {
            return _identityService.GetMayorOfficeUserIds(request.MayorOfficeId);
        }
    }
}
