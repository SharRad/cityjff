﻿using Application.Common.Interfaces;
using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Admins.Queries.GetAdmins
{
    public class GetAdminsByIdQuery : IRequest<AdminDTO>
    {
        public int Id { get; set; }
    }

    public class GetAdminsByIdQueryHandler : IRequestHandler<GetAdminsByIdQuery, AdminDTO>
    {
        private readonly IIdentityService _identityService;
        private readonly IMapper _mapper;

        public GetAdminsByIdQueryHandler(IIdentityService identityService, IMapper mapper)
        {
            _identityService = identityService;
            _mapper = mapper;
        }

        public async Task<AdminDTO> Handle(GetAdminsByIdQuery request, CancellationToken cancellationToken)
        {
            return _mapper.Map<AdminDTO>(await _identityService.GetUserByIdAsync(request.Id));
        }
    }
}