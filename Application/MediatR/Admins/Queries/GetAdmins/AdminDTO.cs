﻿namespace Application.MediatR.Admins.Queries.GetAdmins
{
    public class AdminDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}