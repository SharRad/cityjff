﻿using Application.Common.Interfaces;
using AutoMapper;
using Domain.Enums;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Admins.Queries.GetAdmins
{
    public class GetAdminsQuery : IRequest<List<AdminDTO>>
    {
    }

    public class GetAdminsQueryHandler : IRequestHandler<GetAdminsQuery, List<AdminDTO>>
    {
        private readonly IIdentityService _service;
        private readonly IMapper _mapper;

        public GetAdminsQueryHandler(IIdentityService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        public async Task<List<AdminDTO>> Handle(GetAdminsQuery request, CancellationToken cancellationToken)
        {
            var users = await _service.GetUsersByAclAsync(AccessControlLevel.Administrator);
            return _mapper.Map<List<AdminDTO>>(users);
        }
    }
}