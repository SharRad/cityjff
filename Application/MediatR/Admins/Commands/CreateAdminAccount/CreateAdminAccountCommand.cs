﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Identity;
using Domain.Enums;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Admins.Commands.CreateAdminAccount
{
    public class CreateAdminAccountCommand : IRequest<Result>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int MayorOfficeId { get; set; }
    }

    public class CreateAdminAccountCommandHandler : IRequestHandler<CreateAdminAccountCommand, Result>
    {
        private readonly IIdentityService _identityService;
        private readonly ILogger<CreateAdminAccountCommandHandler> _logger;

        public CreateAdminAccountCommandHandler(IIdentityService identityService, ILogger<CreateAdminAccountCommandHandler> logger)
        {
            _identityService = identityService;
            _logger = logger;
        }

        public async Task<Result> Handle(CreateAdminAccountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var admin = new ApplicationUser
                {
                    UserName = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    UserStatus = UserStatus.Active,
                    IsRoot = false,
                    MayorOfficeId = request.MayorOfficeId
                };

                var (Result, userId) = await _identityService.CreateUserAsync(admin, request.Password, AccessControlLevel.Administrator);

                if (!Result.Succeeded)
                {
                    var exception = new Exception(Result.Errors.ToString());
                    throw exception;
                }
                return Result.Success();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{GetType().Name} failed with error: {ex.Message}");
                return Result.Failure(ApplicationResources.CreationError);
            }
        }
    }
}