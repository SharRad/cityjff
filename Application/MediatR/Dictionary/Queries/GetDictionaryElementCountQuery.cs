﻿using Application.Common.Enums;
using Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Application.MediatR.Dictionary.Queries
{
    public class GetDictionaryElementCountQuery : IRequest<int>
    {
        public DictionaryTypesEnum DictionaryType { get; set; }
    }

    public class GetDictionaryElementCountQueryHandler : IRequestHandler<GetDictionaryElementCountQuery, int>
    {
        private readonly IApplicationDbContext _context;

        public GetDictionaryElementCountQueryHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(GetDictionaryElementCountQuery request, CancellationToken cancellationToken)
        {
            return request.DictionaryType switch
            {
                DictionaryTypesEnum.MayorOffice => await _context.MayorOffices.CountAsync(cancellationToken),
                DictionaryTypesEnum.StructuralUnit => await _context.StructuralUnits.CountAsync(cancellationToken),
                _ => 0
            };
        }
    }
}