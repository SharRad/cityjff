﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Application {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ApplicationResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ApplicationResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Application.ApplicationResources", typeof(ApplicationResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Возникли ошибки во время создания записи.
        /// </summary>
        internal static string CreationError {
            get {
                return ResourceManager.GetString("CreationError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Такие данные уже существуют.
        /// </summary>
        internal static string DataExist {
            get {
                return ResourceManager.GetString("DataExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Возникли ошибки при удалении записи.
        /// </summary>
        internal static string DeleteError {
            get {
                return ResourceManager.GetString("DeleteError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Возникли ошибки во время изменения записи.
        /// </summary>
        internal static string EditError {
            get {
                return ResourceManager.GetString("EditError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Поле является обязательным к заполнению.
        /// </summary>
        internal static string FieldRequiredValidation {
            get {
                return ResourceManager.GetString("FieldRequiredValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Список не должен быть пустым.
        /// </summary>
        internal static string ListRequired {
            get {
                return ResourceManager.GetString("ListRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Значение не должно превышать {0} символов.
        /// </summary>
        internal static string MaxLengthValidation {
            get {
                return ResourceManager.GetString("MaxLengthValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Значение не должно быть меньше {0} символов.
        /// </summary>
        internal static string MinimumLengthValidation {
            get {
                return ResourceManager.GetString("MinimumLengthValidation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Пароли должны совпадать.
        /// </summary>
        internal static string PasswordConfirm {
            get {
                return ResourceManager.GetString("PasswordConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Запись не найдена.
        /// </summary>
        internal static string RecordNotFoundError {
            get {
                return ResourceManager.GetString("RecordNotFoundError", resourceCulture);
            }
        }
    }
}
