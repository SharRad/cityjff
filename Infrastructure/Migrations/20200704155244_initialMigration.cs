﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Infrastructure.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ActiveDateFrom = table.Column<DateTime>(nullable: false),
                    ActiveDateTo = table.Column<DateTime>(nullable: true),
                    Acl = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MayorOffices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'100', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    NameKg = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MayorOffices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NewsNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<int>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    MessageBody = table.Column<string>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    FileUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsNotifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RTokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: false),
                    RefreshToken = table.Column<string>(nullable: true),
                    IsStop = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RTokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TokenFirebases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'100', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: true),
                    Token = table.Column<string>(maxLength: 100, nullable: false),
                    NotifyEnabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TokenFirebases", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StructuralUnits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'100', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    NameKg = table.Column<string>(nullable: true),
                    MayorOfficeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StructuralUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StructuralUnits_MayorOffices_MayorOfficeId",
                        column: x => x.MayorOfficeId,
                        principalTable: "MayorOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CitizenComplaints",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'100', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<int>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: false),
                    MayorOfficeId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(maxLength: 200, nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    GeolocationImagePath = table.Column<string>(nullable: false),
                    TokenFirebaseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CitizenComplaints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CitizenComplaints_MayorOffices_MayorOfficeId",
                        column: x => x.MayorOfficeId,
                        principalTable: "MayorOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CitizenComplaints_TokenFirebases_TokenFirebaseId",
                        column: x => x.TokenFirebaseId,
                        principalTable: "TokenFirebases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'100', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    IsRoot = table.Column<bool>(nullable: false),
                    UserStatus = table.Column<int>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    BanEndDateTime = table.Column<DateTime>(nullable: true),
                    MayorOfficeId = table.Column<int>(nullable: true),
                    StructuralUnitId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_MayorOffices_MayorOfficeId",
                        column: x => x.MayorOfficeId,
                        principalTable: "MayorOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_StructuralUnits_StructuralUnitId",
                        column: x => x.StructuralUnitId,
                        principalTable: "StructuralUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActionHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ComplaintId = table.Column<int>(nullable: false),
                    UserTitle = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Status = table.Column<int>(nullable: false),
                    ActionDate = table.Column<DateTime>(nullable: false),
                    IsRedirected = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActionHistories_CitizenComplaints_ComplaintId",
                        column: x => x.ComplaintId,
                        principalTable: "CitizenComplaints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CitizenComplaintImage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FileUrl = table.Column<string>(nullable: false),
                    FileName = table.Column<string>(nullable: true),
                    FileExt = table.Column<string>(nullable: true),
                    CitizenComplaintId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CitizenComplaintImage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CitizenComplaintImage_CitizenComplaints_CitizenComplaintId",
                        column: x => x.CitizenComplaintId,
                        principalTable: "CitizenComplaints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComplaintNotifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'100', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NotificationType = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 100, nullable: true),
                    Message = table.Column<string>(maxLength: 250, nullable: true),
                    MayorOfficeId = table.Column<int>(nullable: true),
                    StructuralUnitId = table.Column<int>(nullable: true),
                    CreatedDateTime = table.Column<DateTime>(nullable: false),
                    CitizentComplaintId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplaintNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComplaintNotifications_CitizenComplaints_CitizentComplaintId",
                        column: x => x.CitizentComplaintId,
                        principalTable: "CitizenComplaints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ComplaintNotifications_MayorOffices_MayorOfficeId",
                        column: x => x.MayorOfficeId,
                        principalTable: "MayorOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ComplaintNotifications_StructuralUnits_StructuralUnitId",
                        column: x => x.StructuralUnitId,
                        principalTable: "StructuralUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ComplaintStructuralUnits",
                columns: table => new
                {
                    CitizenComplaintId = table.Column<int>(nullable: false),
                    StructuralUnitId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComplaintStructuralUnits", x => new { x.CitizenComplaintId, x.StructuralUnitId });
                    table.ForeignKey(
                        name: "FK_ComplaintStructuralUnits_CitizenComplaints_CitizenComplaint~",
                        column: x => x.CitizenComplaintId,
                        principalTable: "CitizenComplaints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ComplaintStructuralUnits_StructuralUnits_StructuralUnitId",
                        column: x => x.StructuralUnitId,
                        principalTable: "StructuralUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "Acl", "ActiveDateFrom", "ActiveDateTo", "ConcurrencyStamp", "IsActive", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, 60, new DateTime(2020, 7, 4, 21, 52, 43, 685, DateTimeKind.Local).AddTicks(456), null, "943926df-c3f3-4c0d-92c8-b0eab8c75e9b", true, "Administrator", "ADMINISTRATOR" },
                    { 2, 20, new DateTime(2020, 7, 4, 21, 52, 43, 685, DateTimeKind.Local).AddTicks(2767), null, "c0b841d3-a870-4bb7-9023-3cd468af3574", true, "Manager", "MANAGER" },
                    { 3, 10, new DateTime(2020, 7, 4, 21, 52, 43, 685, DateTimeKind.Local).AddTicks(2803), null, "137403da-95a5-4354-8143-d90405e2ecf1", true, "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "BanEndDateTime", "ConcurrencyStamp", "DateOfBirth", "Email", "EmailConfirmed", "FirstName", "Gender", "IsRoot", "LastName", "LockoutEnabled", "LockoutEnd", "MayorOfficeId", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "StructuralUnitId", "TwoFactorEnabled", "UserName", "UserStatus" },
                values: new object[,]
                {
                    { 1, 0, null, "56789fa2-9bad-45d2-ac8c-8f68cc8bb06a", new DateTime(1995, 7, 4, 21, 52, 43, 669, DateTimeKind.Local).AddTicks(1495), "root@root.com", false, "root@root.com", 1, true, "root@root.com", false, null, null, "ROOT@ROOT.COM", "ROOT@ROOT.COM", "AQAAAAEAACcQAAAAEBjj7sL+S1MR1x4Q5fPvYVRp5MdvPbqpzPERRvzHAsEg9IEseUryb+Y9/Q39udeJSw==", null, false, "9c7214c0-f7c9-44d6-81ed-3ae620fccecb", null, false, "root@root.com", 1 },
                    { 2, 0, null, "e576a8e7-6dfb-4f9f-ad05-0fc9048c410a", new DateTime(1995, 7, 4, 21, 52, 43, 678, DateTimeKind.Local).AddTicks(3543), "admin@city.com", false, "admin@city.com", 1, false, "admin@city.com", false, null, null, "ADMIN@CITY.COM", "ADMIN@CITY.COM", "AQAAAAEAACcQAAAAELiBetG03KgOhDkL0ijJk6S23n7qEC8jdIkEIb3Cnz2eLDzhN6fQtGxHb4N+gim3OQ==", null, false, "8a98584d-a6c8-48a3-ae84-b64f86a3f259", null, false, "admin@city.com", 1 }
                });

            migrationBuilder.InsertData(
                table: "MayorOffices",
                columns: new[] { "Id", "Name", "NameKg" },
                values: new object[,]
                {
                    { 1, "Мэрия г.Бишкек", null },
                    { 2, "Мэрия г.Каракол", null },
                    { 3, "Мэрия г.Чолпон ата", null },
                    { 4, "Мэрия г.Балыкчы", null },
                    { 5, "Мэрия г.Талас", null },
                    { 6, "Мэрия г.Нарын", null },
                    { 7, "Мэрия г.Джалал-Абад", null },
                    { 8, "Мэрия г.Ош", null },
                    { 9, "Мэрия г.Баткен", null },
                    { 10, "Мэрия г.Токмок", null }
                });

            migrationBuilder.InsertData(
                table: "TokenFirebases",
                columns: new[] { "Id", "NotifyEnabled", "Token", "UserId" },
                values: new object[] { 1, true, "test token", null });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 }
                });

            migrationBuilder.InsertData(
                table: "CitizenComplaints",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "Description", "GeolocationImagePath", "LastModified", "LastModifiedBy", "Latitude", "Longitude", "MayorOfficeId", "Status", "TokenFirebaseId" },
                values: new object[,]
                {
                    { 1, "Белинского/Московская", new DateTime(2020, 7, 4, 21, 52, 43, 661, DateTimeKind.Local).AddTicks(5661), 2, "Проблема с дорогами", "test", null, 0, 0.0, 0.0, 1, 1, 1 },
                    { 2, "Чуй/Исанова", new DateTime(2020, 7, 4, 21, 52, 43, 662, DateTimeKind.Local).AddTicks(3500), 2, "Проблема с тратуаром", "test", null, 0, 0.0, 0.0, 1, 2, 1 },
                    { 3, "Осипенко/Турусбекова", new DateTime(2020, 7, 4, 21, 52, 43, 662, DateTimeKind.Local).AddTicks(3532), 2, "Проблема с мусором", "test", null, 0, 0.0, 0.0, 1, 4, 1 },
                    { 4, "Киевская/Панфилова", new DateTime(2020, 7, 4, 21, 52, 43, 662, DateTimeKind.Local).AddTicks(3535), 2, "Проблема с освещением", "test", null, 0, 0.0, 0.0, 1, 5, 1 }
                });

            migrationBuilder.InsertData(
                table: "StructuralUnits",
                columns: new[] { "Id", "MayorOfficeId", "Name", "NameKg" },
                values: new object[,]
                {
                    { 1, 1, "Тазалык", null },
                    { 2, 1, "Зеленстрой", null },
                    { 3, 1, "УМС", null },
                    { 4, 1, "УЗР", null },
                    { 5, 1, "Главархитектура", null }
                });

            migrationBuilder.InsertData(
                table: "ComplaintNotifications",
                columns: new[] { "Id", "CitizentComplaintId", "CreatedDateTime", "MayorOfficeId", "Message", "NotificationType", "StructuralUnitId", "Title" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(5547), 1, "Test message", 1, null, "Test title" },
                    { 2, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6346), 1, "Test message", 1, null, "Test title" },
                    { 3, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6366), 1, "Test message", 1, null, "Test title" },
                    { 4, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6368), 1, "Test message", 1, null, "Test title" },
                    { 5, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6370), 1, "Test message", 1, null, "Test title" },
                    { 6, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6372), 1, "Test message", 3, null, "Test title" },
                    { 7, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6374), 1, "Test message", 3, null, "Test title" },
                    { 8, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6375), 1, "Test message", 3, null, "Test title" },
                    { 9, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6377), 1, "Test message", 3, null, "Test title" },
                    { 10, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6762), null, "Test message", 2, 1, "Test title" },
                    { 11, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6782), null, "Test message", 2, 1, "Test title" },
                    { 12, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6783), null, "Test message", 2, 1, "Test title" },
                    { 13, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6785), null, "Test message", 2, 1, "Test title" },
                    { 14, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6786), null, "Test message", 2, 1, "Test title" },
                    { 15, 1, new DateTime(2020, 7, 4, 21, 52, 43, 663, DateTimeKind.Local).AddTicks(6788), null, "Test message", 2, 1, "Test title" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActionHistories_ComplaintId",
                table: "ActionHistories",
                column: "ComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_MayorOfficeId",
                table: "AspNetUsers",
                column: "MayorOfficeId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_StructuralUnitId",
                table: "AspNetUsers",
                column: "StructuralUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_CitizenComplaintImage_CitizenComplaintId",
                table: "CitizenComplaintImage",
                column: "CitizenComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_CitizenComplaints_MayorOfficeId",
                table: "CitizenComplaints",
                column: "MayorOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_CitizenComplaints_TokenFirebaseId",
                table: "CitizenComplaints",
                column: "TokenFirebaseId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintNotifications_CitizentComplaintId",
                table: "ComplaintNotifications",
                column: "CitizentComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintNotifications_MayorOfficeId",
                table: "ComplaintNotifications",
                column: "MayorOfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintNotifications_StructuralUnitId",
                table: "ComplaintNotifications",
                column: "StructuralUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintStructuralUnits_StructuralUnitId",
                table: "ComplaintStructuralUnits",
                column: "StructuralUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_StructuralUnits_MayorOfficeId",
                table: "StructuralUnits",
                column: "MayorOfficeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActionHistories");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CitizenComplaintImage");

            migrationBuilder.DropTable(
                name: "ComplaintNotifications");

            migrationBuilder.DropTable(
                name: "ComplaintStructuralUnits");

            migrationBuilder.DropTable(
                name: "NewsNotifications");

            migrationBuilder.DropTable(
                name: "RTokens");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "CitizenComplaints");

            migrationBuilder.DropTable(
                name: "StructuralUnits");

            migrationBuilder.DropTable(
                name: "TokenFirebases");

            migrationBuilder.DropTable(
                name: "MayorOffices");
        }
    }
}
