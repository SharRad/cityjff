﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using AutoMapper;
using Domain.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class ActionHistoryService : IActionHistoryService
    {
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _context;

        public ActionHistoryService(IApplicationDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<Result> InsertActionHistory(ActionHistoryDTO dto)
        {
            ActionHistory actionHistory = _mapper.Map<ActionHistory>(dto);
            _context.ActionHistories.Add(actionHistory);
            await _context.SaveChangesAsync(new CancellationToken());
            return Result.Success();
        }

        public async Task<Result> InsertActionHistories(List<ActionHistoryDTO> dtos)
        {
            List<ActionHistory> actionHistories = _mapper.Map<List<ActionHistory>>(dtos);
            _context.ActionHistories.AddRange(actionHistories);
            await _context.SaveChangesAsync(new CancellationToken());
            return Result.Success();
        }
    }
}