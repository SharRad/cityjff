﻿using Application.Common.Interfaces;
using Application.Identity;
using Application.Identity.DTOs;
using AutoMapper;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class RoleService : IRoleService
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;

        public RoleService(RoleManager<ApplicationRole> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
            _mapper = mapper;
        }

        public async Task<RoleDTO> GetRoleByAcl(AccessControlLevel acl)
        {
            return _mapper.Map<RoleDTO>(await _roleManager.Roles.Where(x => x.Acl == acl).FirstOrDefaultAsync());
        }

        public async Task<List<RoleDTO>> GetRoles()
        {
            var entity = await _roleManager.Roles.AsNoTracking().OrderBy(c => c.Id).ToListAsync();
            var dto = _mapper.Map<List<RoleDTO>>(entity);
            return dto;
        }
    }
}