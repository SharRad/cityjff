﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using FirebaseAdmin.Messaging;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class FirebaseService : IFirebaseService
    {
        private readonly IApplicationDbContext _context;

        public FirebaseService(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task InsertUserFirebaseToken(int userId, string token)
        {
            bool isExistToken = await _context.TokenFirebases.AnyAsync(x => x.Token == token && x.UserId == userId);
            if (isExistToken)
                return;
            TokenFirebase tokenFirebase = new TokenFirebase
            {
                NotifyEnabled = true,
                Token = token,
                UserId = userId
            };
            _context.TokenFirebases.Add(tokenFirebase);
            await _context.SaveChangesAsync(new CancellationToken());
        }

        public async Task SendNotification(int userId, string title, string body, FirebaseJsonData jsonData)
        {
            var tokens = await _context.TokenFirebases.Where(x => x.UserId == userId).Select(x => x.Token).ToListAsync();
            if (tokens.Count > 0)
            {
                string json = jsonData?.ToJson();
                var message = new MulticastMessage()
                {
                    Tokens = tokens,
                    Data = new Dictionary<string, string>()
                    {
                        { "data", json }
                    },
                    Notification = new Notification
                    {
                        Title = title,
                        Body = body
                    }
                };
                var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message).ConfigureAwait(true);
            }
        }

        public async Task SendMultiNotifications(string title, string body, FirebaseJsonData jsonData)
        {
            var tokens = await _context.TokenFirebases.AsNoTracking().Select(x => x.Token).ToListAsync();
            if (tokens.Count > 0)
            {
                string json = jsonData?.ToJson();
                var message = new MulticastMessage()
                {
                    Tokens = tokens,
                    Data = new Dictionary<string, string>()
                    {
                        { "data", json }
                    },
                    Notification = new Notification
                    {
                        Title = title,
                        Body = body
                    }
                };
                var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message).ConfigureAwait(true);
            }
        }

        public async Task<bool> GetNotificationEnableStatus(string token, CancellationToken cancellationToken)
        {
            var tokenFirebase = await  _context.TokenFirebases.SingleOrDefaultAsync(x => x.Token == token, cancellationToken);
            return tokenFirebase.NotifyEnabled;
        }

        public async Task<Result> SetNotificationEnableStatus(string token, bool enable)
        {
            bool isExistToken = await _context.TokenFirebases.AnyAsync(x => x.Token == token);
            if (isExistToken)
                return Result.Failure("Токен не найден");
            TokenFirebase tokenFirebase = new TokenFirebase
            {
                NotifyEnabled = enable
            };
            _context.TokenFirebases.Update(tokenFirebase);
            await _context.SaveChangesAsync(new CancellationToken());
            return Result.Success();
        }
    }
}