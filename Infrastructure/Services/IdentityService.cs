﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Identity;
using AutoMapper;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IRoleService _roleService;
        private readonly JwtSettings _jwtSettings;
        private readonly IMapper _mapper;
        private readonly IRtTokenService _rtTokenService;
        private readonly IHttpContextUserService _contextUserService;
        private readonly IFirebaseService _firebaseService;

        public IdentityService(UserManager<ApplicationUser> userManager, IRoleService roleService, IRtTokenService rtTokenService,
            SignInManager<ApplicationUser> signInManager, IMapper mapper, IOptions<JwtSettings> options, IHttpContextUserService contextUserService, IFirebaseService firebaseService)
        {
            _userManager = userManager;
            _roleService = roleService;
            _rtTokenService = rtTokenService;
            _signInManager = signInManager;
            _mapper = mapper;
            _jwtSettings = options.Value;
            _contextUserService = contextUserService;
            _firebaseService = firebaseService;
        }

        public async Task<Result> LoginAsync(string email, string password, bool rememberMe)
        {
            var user = await _userManager.Users.Include(c => c.UserRoles).ThenInclude(c => c.Role)
               .SingleOrDefaultAsync(x => x.Email == email);
            if (user == null)
            {
                return Result.Failure("Неправильный логин или пароль!");
            }
            if (user.UserStatus != UserStatus.Active)
            {
                return Result.Failure("Доступ запрещен. Пользователь не активен.");
            }

            var roleResult = CheckRoleWhenLogin(user);
            if (!roleResult.Succeeded)
            {
                return roleResult;
            }

            var result = await _signInManager.PasswordSignInAsync(email, password, rememberMe, false);
            if (!result.Succeeded)
            {
                return Result.Failure("Неправильный логин или пароль!");
            }

            return Result.Success();
        }

        private Result CheckRoleWhenLogin(ApplicationUser user)
        {
            if (user.UserRoles.Count == 0)
                return Result.Failure("Доступ запрещен.");

            return Result.Success();
        }

        public async Task SignoutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<Result> ApiSignOutAsync(int userId)
        {
            await _signInManager.SignOutAsync();
            var result = await _rtTokenService.StopRefreshToken(userId);
            return result;
        }

        public async Task<string> GetUserNameAsync(int userId)
        {
            var user = await _userManager.Users.FirstAsync(u => u.Id == userId);

            return user.UserName;
        }

        public async Task<(Result Result, int userId)> CreateUserAsync(ApplicationUser user, string password, AccessControlLevel acl)
        {
            var result = await _userManager.CreateAsync(user, password);
            var userRole = await _roleService.GetRoleByAcl(acl);
            var userRoleName = userRole.Name;
            await _userManager.AddToRoleAsync(user, userRoleName);

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<(Result Result, int userId)> DeleteUserAsync(int userId)
        {
            IdentityResult result = new IdentityResult();

            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            if (user != null)
            {
                user.UserStatus = UserStatus.Deleted;

                result = await _userManager.UpdateAsync(user);
            }

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<(Result, ApiUserToken)> Authenticate(UserAuthData userAuth)
        {
            var result = await _signInManager.PasswordSignInAsync(userAuth.Username, userAuth.Password, false, false);

            if (result.Succeeded)
            {
                var user = await _userManager.Users.Include(c => c.UserRoles).ThenInclude(c => c.Role)
                .SingleOrDefaultAsync(x => x.Email == userAuth.Username);

                await _firebaseService.InsertUserFirebaseToken(user.Id, userAuth.FirebaseToken);
                List<Claim> claims = CreateUserClaims(user);
                ApiUserToken token = new ApiUserToken();

                var (rtResult, rtToken) = await _rtTokenService.GenerateRToken(user.Id);

                if (!rtResult.Succeeded)
                    return (rtResult, null);

                token.RtToken = rtToken;
                token.JwtToken = GenerateJSONWebToken(claims);

                return (Result.Success(), token);
            }

            return (Result.Failure("Неправильный логин или пароль!"), null);
        }

        public Task<List<ApplicationUser>> GetRootAdminsAsync() => _userManager.Users.Where(u => u.IsRoot).ToListAsync();

        public async Task<IList<ApplicationUser>> GetUsersByAclAsync(AccessControlLevel acl)
        {
            var roleName = await _roleService.GetRoleByAcl(acl);
            return await _userManager.GetUsersInRoleAsync(roleName.Name);
        }

        public ApplicationUser GetUserByEmail(string email) => _userManager.Users.FirstOrDefault(u => u.Email == email);

        public async Task<UserInfoDTO> GetUserByIdAsync(int id)
        {
            var user = await _userManager.Users.Include(c => c.UserRoles).ThenInclude(c => c.Role)
                .SingleOrDefaultAsync(u => u.Id == id);

            return _mapper.Map<UserInfoDTO>(user);
        }

        public async Task<Result> ChangePassword(ChangePasswordDTO dto)
        {
            var user = await _userManager.Users.Include(c => c.UserRoles).ThenInclude(c => c.Role)
                .SingleOrDefaultAsync(x => x.Id == dto.UserId);
            if (user == null)
                return Result.Failure(new List<string> { "Пользователь не найден." });
            var result = await _userManager.ChangePasswordAsync(user, dto.OldPassword, dto.Password);
            return result.ToApplicationResult();
        }

        public async Task<(Result, string)> GenerateResetPasswordToken(string userMail)
        {
            var user = await _userManager.Users
                    .SingleOrDefaultAsync(x => x.Email == userMail);
            if (user == null)
            {
                return (Result.Failure(new List<string> { "Пользователь не найден." }), null);
            }

            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            return (Result.Success(), code);
        }

        public async Task<Result> ResetUserPassword(string email, string code, string newPwd)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
                return Result.Failure(new List<string> { "Пользователь не найден." });

            var result = await _userManager.ResetPasswordAsync(user, code, newPwd);

            return result.Succeeded
                ? Result.Success()
                : Result.Failure(new List<string> { "Операция не выполнилась" });
        }

        public async Task<(Result, ApiUserToken)> RefreshToken(UserRefreshToken userRefreshData)
        {
            int currentUserId = _contextUserService.UserId;

            var (rtResult, rtToken) = await _rtTokenService.RefreshToken(currentUserId, userRefreshData.RefreshToken);

            if (!rtResult.Succeeded)
                return (rtResult, null);

            var user = await _userManager.Users.Include(c => c.UserRoles).ThenInclude(c => c.Role)
                .SingleOrDefaultAsync(u => u.Id == currentUserId);

            List<Claim> claims = CreateUserClaims(user);

            var token = new ApiUserToken
            {
                JwtToken = GenerateJSONWebToken(claims),
                RtToken = rtToken
            };

            return (Result.Success(), token);
        }

        private List<Claim> CreateUserClaims(ApplicationUser user)
        {
            return new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim("userInfo", JsonConvert.SerializeObject(_mapper.Map<UserInfoDTO>(user)))
            };
        }

        private string GenerateJSONWebToken(List<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Key));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _jwtSettings.Issuer,
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(3),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public async Task<Result> BanAccount(int id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null)
            {
                user.UserStatus = UserStatus.Banned;
                var result = await _userManager.UpdateAsync(user);
                return result.ToApplicationResult();
            }
            return Result.Failure("Пользователь не найден.");
        }

        public async Task<Result> UnBanAccount(int id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user != null)
            {
                user.UserStatus = UserStatus.Active;
                var result = await _userManager.UpdateAsync(user);
                return result.ToApplicationResult();
            }
            return Result.Failure("Пользователь не найден.");
        }

        public Task<List<string>> GetMayorOfficeUserIds(int mayorOfficeId)
        {
            return _userManager.Users
                .Where(u => u.UserStatus == UserStatus.Active && u.MayorOfficeId == mayorOfficeId && u.StructuralUnitId == null)
                .Select(u => u.Id.ToString())
                .ToListAsync();
        }
    }
}