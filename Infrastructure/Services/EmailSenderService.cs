﻿using Application.Common.Interfaces;
using Application.Common.Models;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Utils;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly EmailSetting _emailsSetting;

        public EmailSenderService(IOptions<EmailSetting> options)
        {
            _emailsSetting = options.Value;
        }

        public async Task SendEmailAsync(EmailMessage emailMessage)
        {
            using SmtpClient client = new SmtpClient();
            await client.ConnectAsync(_emailsSetting.Host, _emailsSetting.Port, _emailsSetting.EnableSSL);
            await client.AuthenticateAsync(_emailsSetting.Login, _emailsSetting.Password);
            await client.SendAsync(SetMimeMessage(emailMessage));
            await client.DisconnectAsync(true);
        }

        private MimeMessage SetMimeMessage(EmailMessage emailMessage)
        {
            MimeMessage mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress("Администрация Shipping", _emailsSetting.FromEmailAddress));
            mimeMessage.To.Add(new MailboxAddress(emailMessage.ToEmailAddress, emailMessage.ToEmailAddress));
            mimeMessage.Subject = emailMessage.Subject + $" {emailMessage.ToEmailAddress}";
            mimeMessage.Importance = MessageImportance.High;
            mimeMessage.MessageId = MimeUtils.GenerateMessageId();
            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = $"<h4>Уважаемый {emailMessage.ToEmailAddress},</h4><br>" + emailMessage.Content };
            return mimeMessage;
        }
    }
}