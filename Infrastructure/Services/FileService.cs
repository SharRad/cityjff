﻿using Application.Common.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class FileService : IFileService
    {
        private readonly IWebHostEnvironment _environment;
        private const string FILES_DIR = "Files";
        private const string GEO_FILES_DIR = "GEOImages";

        public FileService(IWebHostEnvironment environment)
        {
            _environment = environment;
        }

        public async Task<string> AddFileAsync(IFormFile file)
        {
            if (file == null)
                return String.Empty;

            string imageId = Guid.NewGuid().ToString();
            string extension = Path.GetExtension(file.FileName);
            string relativePath = $"/{FILES_DIR}/{imageId}{extension}";
            string folderPath = $"{_environment.WebRootPath}/{FILES_DIR}";
            string physicalPath = $"{_environment.WebRootPath}{relativePath}";

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            using var stream = new FileStream(physicalPath, FileMode.Create);
            await file.CopyToAsync(stream);

            return relativePath;
        }

        public async Task<List<string>> AddFilesAsync(IFormFileCollection files)
        {
            if (files == null || files.Count == 0)
                return null;

            var pathes = new List<string>();

            foreach (var file in files)
            {
                string imageId = Guid.NewGuid().ToString();
                string extension = Path.GetExtension(file.FileName);
                string relativePath = $"/{FILES_DIR}/{imageId}{extension}";
                string folderPath = $"{_environment.WebRootPath}/{FILES_DIR}";
                string physicalPath = $"{_environment.WebRootPath}{relativePath}";

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);

                using var stream = new FileStream(physicalPath, FileMode.Create);
                await file.CopyToAsync(stream);

                pathes.Add(relativePath);
            }

            return pathes;
        }

        public string AddGeoFile(double latitude, double longitude)
        {
            using WebClient client = new WebClient();
            string url = $"https://open.mapquestapi.com/staticmap/v5/map?key=hUEuGI5cb0qddbKm7OL8NAoAXl1oFfMl&locations={latitude},{longitude}|marker-7B0099&zoom=16&size=800,550";
            string imageId = Guid.NewGuid().ToString();
            string folderFileName = $"/{GEO_FILES_DIR}/{imageId}.png";
            string path = _environment.WebRootPath + $"/{GEO_FILES_DIR}/";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            client.DownloadFileAsync(new Uri(url), path + $"{imageId}.png");
            return folderFileName;
        }
    }
}