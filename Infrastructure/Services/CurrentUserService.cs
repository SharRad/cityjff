﻿using Application.Common.DTOs;
using Application.Common.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Security.Claims;

namespace Infrastructure.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public UserInfoDTO Get { get; set; }

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            string userData = httpContextAccessor.HttpContext?.User?.FindFirstValue("userInfo");
            if (!string.IsNullOrEmpty(userData))
                Get = JsonConvert.DeserializeObject<UserInfoDTO>(userData);
        }
    }
}