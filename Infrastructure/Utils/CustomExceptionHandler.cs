﻿using Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace Infrastructure.Utils
{
    public class CustomExceptionHandler : ICustomExceptionHandler
    {
        public string CatchDbException(DbUpdateException exception)
        {
            var sqlException = exception.GetBaseException() as PostgresException;

            if (sqlException != null)
            {
                var state = sqlException.SqlState;

                if (state == "23503")
                {
                    return "Удаление невозможно из-за существующих связей!";
                }
            }
            return "Ошибка базы данных!";
        }
    }
}