﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.Configuration
{
    public class ComplaintNotificationConfiguration : IEntityTypeConfiguration<ComplaintNotification>
    {
        public void Configure(EntityTypeBuilder<ComplaintNotification> builder)
        {
            builder.Property(n => n.NotificationType).IsRequired();
            builder.Property(n => n.Title).HasMaxLength(100);
            builder.Property(n => n.Message).HasMaxLength(250);
            builder.Property(n => n.CreatedDateTime).IsRequired();
            builder.HasOne(n => n.MayorOffice).WithMany(m => m.ComplaintNotifications).HasForeignKey(n => n.MayorOfficeId);
            builder.HasOne(n => n.StructuralUnit).WithMany(s => s.ComplaintNotifications).HasForeignKey(n => n.StructuralUnitId);
            builder.HasOne(n => n.CitizenComplaint).WithMany(c => c.ComplaintNotifications).HasForeignKey(n => n.CitizentComplaintId);
        }
    }
}
