﻿using Application.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class ApplicationUserRoleConfiguration : IEntityTypeConfiguration<ApplicationUserRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationUserRole> builder)
        {
            builder.HasKey(k => new { k.UserId, k.RoleId });

            builder.HasOne(c => c.Role).WithMany(m => m.UserRoles).HasForeignKey(b => b.RoleId);
            builder.HasOne(c => c.User).WithMany(m => m.UserRoles).HasForeignKey(b => b.UserId);
        }
    }
}