﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class CitizenComplaintConfiguration : IEntityTypeConfiguration<CitizenComplaint>
    {
        public void Configure(EntityTypeBuilder<CitizenComplaint> builder)
        {
            builder.HasOne(b => b.MayorOffice).WithMany(b => b.CitizenComplaints).HasForeignKey(b => b.MayorOfficeId);
            builder.Property(b => b.MayorOfficeId).IsRequired();
            builder.Property(b => b.Status).IsRequired();
            builder.Property(b => b.Description).IsRequired().HasMaxLength(200);
            builder.Property(b => b.Address).IsRequired().HasMaxLength(200);
            builder.Property(b => b.Latitude).IsRequired();
            builder.Property(b => b.Longitude).IsRequired();
            builder.Property(b => b.GeolocationImagePath).IsRequired();
            builder.HasOne(b => b.TokenFirebase).WithMany(f => f.CitizenComplaints).HasForeignKey(b => b.TokenFirebaseId);
        }
    }
}