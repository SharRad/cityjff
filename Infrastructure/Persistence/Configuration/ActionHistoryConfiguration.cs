﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class ActionHistoryConfiguration : IEntityTypeConfiguration<ActionHistory>
    {
        public void Configure(EntityTypeBuilder<ActionHistory> builder)
        {
            builder.HasKey(k => k.Id);

            builder.HasOne(x => x.CitizenComplaint)
                .WithMany(x => x.ActionHistories)
                .HasForeignKey(ur => ur.ComplaintId);
            builder.Property(p => p.ActionDate).IsRequired();
            builder.Property(p => p.Status).IsRequired();
            builder.Property(p => p.UserTitle).HasMaxLength(200).IsRequired();
            builder.Property(p => p.Description).HasMaxLength(200);
        }
    }
}