﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class CitizenComplaintImageConfiguration : IEntityTypeConfiguration<CitizenComplaintImage>
    {
        public void Configure(EntityTypeBuilder<CitizenComplaintImage> builder)
        {
            builder.HasOne(b => b.CitizenComplaint).WithMany(b => b.CitizenComplaintImages).HasForeignKey(b => b.CitizenComplaintId);
            builder.Property(b => b.FileUrl).IsRequired();
        }
    }
}