﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class StructuralUnitConfiguration : IEntityTypeConfiguration<StructuralUnit>
    {
        public void Configure(EntityTypeBuilder<StructuralUnit> builder)
        {
            builder.HasOne(b => b.MayorOffice).WithMany(b => b.StructuralUnits).HasForeignKey(b => b.MayorOfficeId);
            builder.Property(b => b.Name).IsRequired().HasMaxLength(200);
        }
    }
}