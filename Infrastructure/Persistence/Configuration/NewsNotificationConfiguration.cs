﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class NewsNotificationConfiguration : IEntityTypeConfiguration<NewsNotification>
    {
        public void Configure(EntityTypeBuilder<NewsNotification> builder)
        {
            builder.HasKey(k => k.Id);

            builder.Property(p => p.Title).IsRequired();
            builder.Property(p => p.MessageBody).IsRequired();
        }
    }
}