﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class TokenFirebaseConfiguration : IEntityTypeConfiguration<TokenFirebase>
    {
        public void Configure(EntityTypeBuilder<TokenFirebase> builder)
        {
            builder.Property(b => b.Token).IsRequired().HasMaxLength(100);
            builder.Property(b => b.NotifyEnabled).IsRequired();
        }
    }
}