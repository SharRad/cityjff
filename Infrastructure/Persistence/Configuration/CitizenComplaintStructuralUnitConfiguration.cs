﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class CitizenComplaintStructuralUnitConfiguration : IEntityTypeConfiguration<CitizenComplaintStructuralUnit>
    {
        public void Configure(EntityTypeBuilder<CitizenComplaintStructuralUnit> builder)
        {
            builder.HasKey(k => new { k.CitizenComplaintId, k.StructuralUnitId });

            builder.HasOne(b => b.CitizenComplaint)
                .WithMany(b => b.CitizenComplaintStructuralUnits)
                .HasForeignKey(b => b.CitizenComplaintId);

            builder.HasOne(b => b.StructuralUnit)
                .WithMany(b => b.CitizenComplaintStructuralUnits)
                .HasForeignKey(b => b.StructuralUnitId);

            builder.Property(b => b.Status).IsRequired();
            builder.Property(b => b.Description).HasMaxLength(500);
        }
    }
}