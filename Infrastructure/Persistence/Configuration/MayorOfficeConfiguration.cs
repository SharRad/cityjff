﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class MayorOfficeConfiguration : IEntityTypeConfiguration<MayorOffice>
    {
        public void Configure(EntityTypeBuilder<MayorOffice> builder)
        {
            builder.Property(b => b.Name).IsRequired().HasMaxLength(200);
        }
    }
}