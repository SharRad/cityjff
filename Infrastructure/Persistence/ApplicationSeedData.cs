﻿using Application.Identity;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace Infrastructure.Persistence
{
    internal static class ApplicationSeedData
    {
        internal static ModelBuilder AddMayorOfficeTestData(this ModelBuilder builder)
        {
            var mayorOffices = new MayorOffice[]
            {
                new MayorOffice { Id = 1, Name = "Мэрия г.Бишкек" },
                new MayorOffice { Id = 2, Name = "Мэрия г.Каракол" },
                new MayorOffice { Id = 3, Name = "Мэрия г.Чолпон ата" },
                new MayorOffice { Id = 4, Name = "Мэрия г.Балыкчы" },
                new MayorOffice { Id = 5, Name = "Мэрия г.Талас" },
                new MayorOffice { Id = 6, Name = "Мэрия г.Нарын" },
                new MayorOffice { Id = 7, Name = "Мэрия г.Джалал-Абад" },
                new MayorOffice { Id = 8, Name = "Мэрия г.Ош" },
                new MayorOffice { Id = 9, Name = "Мэрия г.Баткен" },
                new MayorOffice { Id = 10, Name = "Мэрия г.Токмок" },
            };

            builder.Entity<MayorOffice>().HasData(mayorOffices);

            return builder;
        }

        internal static ModelBuilder AddStructuralUnitTestData(this ModelBuilder builder)
        {
            var structuralUnits = new StructuralUnit[]
            {
                new StructuralUnit { Id = 1, Name = "Тазалык", MayorOfficeId = 1 },
                new StructuralUnit { Id = 2, Name = "Зеленстрой", MayorOfficeId = 1 },
                new StructuralUnit { Id = 3, Name = "УМС", MayorOfficeId = 1 },
                new StructuralUnit { Id = 4, Name = "УЗР", MayorOfficeId = 1 },
                new StructuralUnit { Id = 5, Name = "Главархитектура", MayorOfficeId = 1 }
            };

            builder.Entity<StructuralUnit>().HasData(structuralUnits);

            return builder;
        }

        internal static ModelBuilder AddCitizenComplaintTestData(this ModelBuilder builder)
        {
            var citizenComplaints = new CitizenComplaint[]
            {
                new CitizenComplaint
                {
                    Id = 1,
                    Status = CitizenComplaintStatus.New,
                    MayorOfficeId = 1,
                    Description = "Проблема с дорогами",
                    Address = "Белинского/Московская",
                    GeolocationImagePath = "test",
                    TokenFirebaseId = 1,
                    CreatedBy = 2,
                    Created = DateTime.Now
                },
                new CitizenComplaint
                {
                    Id = 2,
                    Status = CitizenComplaintStatus.InProgress,
                    MayorOfficeId = 1,
                    Description = "Проблема с тратуаром",
                    Address = "Чуй/Исанова",
                    GeolocationImagePath = "test",
                    TokenFirebaseId = 1,
                    CreatedBy = 2,
                    Created = DateTime.Now
                },
                new CitizenComplaint
                {
                    Id = 3,
                    Status = CitizenComplaintStatus.Completed,
                    MayorOfficeId = 1,
                    Description = "Проблема с мусором",
                    Address = "Осипенко/Турусбекова",
                    GeolocationImagePath = "test",
                    TokenFirebaseId = 1,
                    CreatedBy = 2,
                    Created = DateTime.Now
                },
                new CitizenComplaint
                {
                    Id = 4,
                    Status = CitizenComplaintStatus.Canceled,
                    MayorOfficeId = 1,
                    Description = "Проблема с освещением",
                    Address = "Киевская/Панфилова",
                    GeolocationImagePath = "test",
                    TokenFirebaseId = 1,
                    CreatedBy = 2,
                    Created = DateTime.Now
                },
            };

            builder.Entity<CitizenComplaint>().HasData(citizenComplaints);

            return builder;
        }

        internal static ModelBuilder SetPKAutoIncrementNumber(this ModelBuilder builder)
        {
            const int PK_INCREMENT_FROM = 100;

            builder.Entity<MayorOffice>().Property(b => b.Id).HasIdentityOptions(startValue: PK_INCREMENT_FROM);
            builder.Entity<StructuralUnit>().Property(b => b.Id).HasIdentityOptions(startValue: PK_INCREMENT_FROM);
            builder.Entity<CitizenComplaint>().Property(b => b.Id).HasIdentityOptions(startValue: PK_INCREMENT_FROM);
            builder.Entity<TokenFirebase>().Property(b => b.Id).HasIdentityOptions(startValue: PK_INCREMENT_FROM);
            builder.Entity<ApplicationUser>().Property(b => b.Id).HasIdentityOptions(startValue: PK_INCREMENT_FROM);
            builder.Entity<ComplaintNotification>().Property(b => b.Id).HasIdentityOptions(startValue: PK_INCREMENT_FROM);

            return builder;
        }

        internal static ModelBuilder AddRoot(this ModelBuilder builder)
        {
            const string userName = "root@root.com";
            const string password = "root123";
            int Id = 1;

            var user = new ApplicationUser
            {
                Id = Id,
                UserName = userName,
                Email = userName,
                FirstName = userName,
                LastName = userName,
                DateOfBirth = DateTime.Now.AddYears(-25),
                Gender = Gender.Male,
                NormalizedEmail = userName.ToUpper(),
                NormalizedUserName = userName.ToUpper(),

                SecurityStamp = Guid.NewGuid().ToString(),
                ConcurrencyStamp = Guid.NewGuid().ToString(),

                IsRoot = true,
                UserStatus = UserStatus.Active
            };
            user.PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(user, password);

            builder.Entity<ApplicationUser>().HasData(user);

            ApplicationUserRole role = new ApplicationUserRole
            { RoleId = 1, UserId = Id };

            builder.Entity<ApplicationUserRole>().HasData(role);
            return builder;
        }

        internal static ModelBuilder AddDefaultAdmin(this ModelBuilder builder)
        {
            const string userName = "admin@city.com";
            const string password = "admin123";
            int Id = 2;

            var user = new ApplicationUser
            {
                Id = Id,
                UserName = userName,
                Email = userName,
                FirstName = userName,
                LastName = userName,
                DateOfBirth = DateTime.Now.AddYears(-25),
                Gender = Gender.Male,
                NormalizedEmail = userName.ToUpper(),
                NormalizedUserName = userName.ToUpper(),

                SecurityStamp = Guid.NewGuid().ToString(),
                ConcurrencyStamp = Guid.NewGuid().ToString(),

                IsRoot = false,
                UserStatus = UserStatus.Active
            };
            user.PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(user, password);

            builder.Entity<ApplicationUser>().HasData(user);

            ApplicationUserRole role = new ApplicationUserRole
            { RoleId = 1, UserId = Id };

            builder.Entity<ApplicationUserRole>().HasData(role);

            return builder;
        }

        internal static ModelBuilder AddRoles(this ModelBuilder builder)
        {
            builder.Entity<ApplicationRole>().HasData(
               new ApplicationRole[]
               {
                    new ApplicationRole {
                        Id = 1,
                        Acl = AccessControlLevel.Administrator,
                        ActiveDateFrom = DateTime.Now,
                        ConcurrencyStamp = Guid.NewGuid().ToString(),
                        IsActive = true, Name = "Administrator",
                        NormalizedName = "Administrator".ToUpper(),
                        ActiveDateTo = null
                    },
                    new ApplicationRole {
                        Id = 2,
                        Acl = AccessControlLevel.Manager,
                        ActiveDateFrom = DateTime.Now,
                        ConcurrencyStamp = Guid.NewGuid().ToString(),
                        IsActive = true,
                        Name = "Manager",
                        NormalizedName = "Manager".ToUpper(),
                        ActiveDateTo = null
                    },
                    new ApplicationRole {
                        Id = 3,
                        Acl = AccessControlLevel.User,
                        ActiveDateFrom = DateTime.Now,
                        ConcurrencyStamp = Guid.NewGuid().ToString(),
                        IsActive = true,
                        Name = "User",
                        NormalizedName = "User".ToUpper(),
                        ActiveDateTo = null
                    },
               });
            return builder;
        }

        internal static ModelBuilder AddFirebaseTestData(this ModelBuilder builder)
        {
            builder.Entity<TokenFirebase>().HasData(new TokenFirebase { Id = 1, Token = "test token" });

            return builder;
        }


        internal static ModelBuilder AddComplaintNotificationTestData(this ModelBuilder builder)
        {
            var notifications = new ComplaintNotification[]
            {
                new ComplaintNotification 
                { 
                    Id = 1, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.NewAdmin, 
                    Title = "Test title", Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 2, MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.NewAdmin, 
                    Title = "Test title", Message = "Test message",
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 3,
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.NewAdmin, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 4, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.NewAdmin, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 5, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.NewAdmin, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },

                new ComplaintNotification 
                { 
                    Id = 6, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.CanceledManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 7, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.CanceledManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 8, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.CanceledManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 9, 
                    MayorOfficeId = 1, 
                    NotificationType = ComplaintNotificationType.CanceledManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },

                new ComplaintNotification 
                { 
                    Id = 10, 
                    StructuralUnitId = 1, 
                    NotificationType = ComplaintNotificationType.NewManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 11, 
                    StructuralUnitId = 1, 
                    NotificationType = ComplaintNotificationType.NewManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 12, 
                    StructuralUnitId = 1,
                    NotificationType = ComplaintNotificationType.NewManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 13, 
                    StructuralUnitId = 1, 
                    NotificationType = ComplaintNotificationType.NewManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 14, 
                    StructuralUnitId = 1, 
                    NotificationType = ComplaintNotificationType.NewManager, 
                    Title = "Test title", 
                    Message = "Test message", 
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
                new ComplaintNotification 
                { 
                    Id = 15, 
                    StructuralUnitId = 1, 
                    NotificationType = ComplaintNotificationType.NewManager, 
                    Title = "Test title",
                    Message = "Test message",
                    CreatedDateTime = DateTime.Now,
                    CitizentComplaintId = 1
                },
            };

            builder.Entity<ComplaintNotification>().HasData(notifications);

            return builder;
        }
    }
}