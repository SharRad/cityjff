﻿using Application.Common.Interfaces;
using Application.Identity;
using Domain.Common;
using Domain.Entities;
using Infrastructure.Persistence.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Reflection;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, IdentityUserClaim<int>, ApplicationUserRole,
        IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>, IApplicationDbContext
    {
        private IDbContextTransaction _currentTransaction;
        private readonly IDateTime _dateTime;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options,
            IDateTime dateTime) : base(options)
        {
            _dateTime = dateTime;
        }

        public DbSet<MayorOffice> MayorOffices { get; set; }
        public DbSet<StructuralUnit> StructuralUnits { get; set; }
        public DbSet<CitizenComplaint> CitizenComplaints { get; set; }
        public DbSet<CitizenComplaintStructuralUnit> ComplaintStructuralUnits { get; set; }

        public DbSet<TokenFirebase> TokenFirebases { get; set; }
        public DbSet<RToken> RTokens { get; set; }

        public DbSet<ActionHistory> ActionHistories { get; set; }

        public DbSet<NewsNotification> NewsNotifications { get; set; }

        public DbSet<ComplaintNotification> ComplaintNotifications { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken token = new CancellationToken())
        {
            int userId = 0;
            var httpContextAccessor = this.GetService<IHttpContextAccessor>();
            string userIdVal = httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier);
            if (!string.IsNullOrEmpty(userIdVal))
                userId = Convert.ToInt32(userIdVal);

            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = userId; //_contextUserService.UserId;
                        entry.Entity.Created = _dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = userId; //_contextUserService.UserId;
                        entry.Entity.LastModified = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(token);
        }

        public async Task BeginTransactionAsync()
        {
            if (_currentTransaction != null)
                return;

            _currentTransaction = await base.Database.BeginTransactionAsync(IsolationLevel.ReadCommitted).ConfigureAwait(false);
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync().ConfigureAwait(false);

                await _currentTransaction?.CommitAsync();
            }
            catch
            {
                await RollbackTransactionAsync();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public async Task RollbackTransactionAsync()
        {
            try
            {
                await _currentTransaction?.RollbackAsync();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new ApplicationUserRoleConfiguration());

            // seed data
            builder.AddMayorOfficeTestData();
            builder.AddStructuralUnitTestData();
            builder.AddCitizenComplaintTestData();
            builder.AddFirebaseTestData();
            builder.AddComplaintNotificationTestData();

            // set pk autoincrement number
            builder.SetPKAutoIncrementNumber();

            builder.AddRoot();
            builder.AddDefaultAdmin();
            builder.AddRoles();
        }
    }
}