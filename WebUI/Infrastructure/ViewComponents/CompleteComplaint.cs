﻿using Application.MediatR.CitizenComplaints.Commands.EditCitizenComplaint;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class CompleteComplaint : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            EditCitizenComplaintStatusCommand command = new EditCitizenComplaintStatusCommand();
            return View(command);
        }
    }
}