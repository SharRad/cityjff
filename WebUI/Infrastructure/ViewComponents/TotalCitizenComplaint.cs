﻿using Application.MediatR.Statistics.Queries.GetTotalCitizenComplaints;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class TotalCitizenComplaint : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var totalCitizenComplaints = await Mediator.Send(new GetTotalCitizenComplaintsQuery());

            return View(totalCitizenComplaints);
        }
    }
}