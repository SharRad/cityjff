﻿using Application.MediatR.DivisionCitizenComplaints.Commands.CancelDivisionComplaint;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class CancelDivisionComplaint : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            CancelDivisionComplaintCommand command = new CancelDivisionComplaintCommand();
            return View(command);
        }
    }
}