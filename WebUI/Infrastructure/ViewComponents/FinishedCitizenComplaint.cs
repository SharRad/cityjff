﻿using Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaints;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class FinishedCitizenComplaint : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var finishedCitizenComplaints = await Mediator.Send(new GetFinishedCitizenComplaintsQuery());

            return View(finishedCitizenComplaints);
        }
    }
}