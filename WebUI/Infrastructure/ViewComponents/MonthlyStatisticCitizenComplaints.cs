﻿using Application.MediatR.Statistics.Queries.GetCitizenComplaintsByMonthly;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class MonthlyStatisticCitizenComplaints : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var citizenComplaintsByMonthes = await Mediator.Send(new GetCitizenComplaintsByMonthlyQuery());

            return View(citizenComplaintsByMonthes);
        }
    }
}