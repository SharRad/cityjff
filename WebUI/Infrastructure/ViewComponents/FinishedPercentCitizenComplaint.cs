﻿using Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaintsByPercent;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class FinishedPercentCitizenComplaint : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            double finishedPercentComplaints = await Mediator.Send(new GetFinishedCitizenComplaintsByPercentQuery());

            return View(finishedPercentComplaints);
        }
    }
}