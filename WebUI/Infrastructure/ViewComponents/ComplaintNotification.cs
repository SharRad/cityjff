﻿using Application.MediatR.ComplaintNotifications.Queries.GetComplaintNotificationsCountQuery;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class ComplaintNotification : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var complaintNotificationVm = await Mediator.Send(new GetCompalintNotificationsCountQuery());

            return View(complaintNotificationVm);
        }
    }
}
