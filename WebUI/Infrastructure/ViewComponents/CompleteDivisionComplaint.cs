﻿using Application.MediatR.DivisionCitizenComplaints.Commands.CompleteDivivsionComplaint;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.ViewComponents
{
    public class CompleteDivisionComplaint : BaseViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            CompleteDivisionComplaintCommand command = new CompleteDivisionComplaintCommand();
            return View(command);
        }
    }
}