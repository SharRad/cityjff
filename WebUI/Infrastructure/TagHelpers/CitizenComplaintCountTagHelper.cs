﻿using Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints;
using MediatR;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.TagHelpers
{
    public class CitizenComplaintCountTagHelper : TagHelper
    {
        private IMediator _mediator;

        public CitizenComplaintCountTagHelper(IMediator mediator)
        {
            _mediator = mediator;
        }

        public string Class { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            int count = await _mediator.Send(new GetCitizenComplaintCountQuery());

            output.TagName = "div";
            output.Attributes.SetAttribute("class", Class);
            output.Content.SetContent(count.ToString());

            await base.ProcessAsync(context, output);
        }
    }
}