﻿using Application.Common.Enums;
using Application.MediatR.Dictionary.Queries;
using MediatR;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.TagHelpers
{
    public class DictionaryElementCountTagHelper : TagHelper
    {
        private IMediator _mediator;

        public DictionaryElementCountTagHelper(IMediator mediator)
        {
            _mediator = mediator;
        }

        public string Class { get; set; }
        public DictionaryTypesEnum DictionaryType { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            int count = await _mediator.Send(new GetDictionaryElementCountQuery { DictionaryType = DictionaryType });

            output.TagName = "div";
            output.Attributes.SetAttribute("class", Class);
            output.Content.SetContent(count.ToString());

            await base.ProcessAsync(context, output);
        }
    }
}