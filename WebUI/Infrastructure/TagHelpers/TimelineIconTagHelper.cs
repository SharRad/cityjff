﻿using Domain.Enums;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.TagHelpers
{
    public class TimelineIconTagHelper : TagHelper
    {
        public CitizenComplaintStatus Status { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            (string divClass, string iClass) = GetStatusInformation();

            output.TagName = "div";
            output.Attributes.SetAttribute("class", $"{divClass} vertical-timeline-icon");
            output.Content.SetHtmlContent($"<i class=\"{iClass}\"></i>");

            await base.ProcessAsync(context, output);

            (string, string) GetStatusInformation()
            {
                return Status switch
                {
                    CitizenComplaintStatus.New => ("lazur-bg", "mdi mdi-email-newsletter"),
                    CitizenComplaintStatus.InProgress => ("blue-bg", "mdi mdi-email-search"),
                    CitizenComplaintStatus.ReadyToComplete => ("yellow-bg", "mdi mdi-email-alert"),
                    CitizenComplaintStatus.Completed => ("navy-bg", "mdi mdi-email-check"),
                    CitizenComplaintStatus.Canceled => ("red-bg", "mdi mdi-reply-all-outline"),
                    _ => ("gray-bg", "")
                };
            }
        }
    }
}