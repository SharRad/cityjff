﻿using Domain.Enums;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace WebUI.Infrastructure.TagHelpers
{
    public class CitizenComplaintStatusTagHelper : TagHelper
    {
        public CitizenComplaintStatus Status { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            (string statusText, string cssClass) = GetStatusInformation();

            output.TagName = "span";
            output.Attributes.SetAttribute("class", $"{cssClass} p-1");
            output.Content.SetContent(statusText);

            await base.ProcessAsync(context, output);

            (string, string) GetStatusInformation()
            {
                return Status switch
                {
                    CitizenComplaintStatus.New => ("Новая", "badge badge-info"),
                    CitizenComplaintStatus.InProgress => ("На выполнении", "badge badge-success"),
                    CitizenComplaintStatus.ReadyToComplete => ("Готова к завершению", "badge badge-warning"),
                    CitizenComplaintStatus.Completed => ("Завершена", "badge badge-primary"),
                    CitizenComplaintStatus.Canceled => ("Отклонена", "badge badge-danger"),
                    _ => ("Статус неизвестен", "badge badge-secondary")
                };
            }
        }
    }
}