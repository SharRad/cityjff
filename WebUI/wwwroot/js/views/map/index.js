﻿var mymap = L.map('mapid', {
    center: [42.875674, 74.611587],
    zoom: 12,
    maxZoom: 18,
    minZoom: 12
});

var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    style: 'mapbox://styles/mapbox/streets-v10',
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

var point = new L.Point(20, 20);

var newIcon = L.divIcon({ className: 'leaflet-new-icon', iconSize: point });
var processIcon = L.divIcon({ className: 'leaflet-process-icon', iconSize: point });
var completedIcon = L.divIcon({ className: 'leaflet-completed-icon', iconSize: point });
var readyIcon = L.divIcon({ className: 'leaflet-ready-icon', iconSize: point });
var cancelIcon = L.divIcon({ className: 'leaflet-cancel-icon', iconSize: point });

var promise = $.getJSON("/Map/LoadMapComplaint");
var promise2 = $.getJSON("/Map/LoadGeoBishkek");
function onEachFeature(feature, layer) {
    layer.on('click', function (e) {
        window.open(
            "/CitizenComplaint/Details?id=" + feature.properties.id, "_blank");
    });
}
function inside(point, vs) {
    // ray-casting algorithm based on
    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

    var x = point[0], y = point[1];

    var inside = false;
    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
        var xi = vs[i][0], yi = vs[i][1];
        var xj = vs[j][0], yj = vs[j][1];

        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
};
var allComplaints;
promise.then(function (data) {
    allComplaints = data.features;
    var newComplaints = L.geoJson(data, {
        onEachFeature: onEachFeature,
        filter: function (feature, layer) {
            return feature.properties.status == 1;
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                icon: newIcon
            }).on('mouseover', function () {
                this.bindPopup(feature.properties.description).openPopup();
            });
        }
    });
    var processComplaints = L.geoJson(data, {
        onEachFeature: onEachFeature,
        filter: function (feature, layer) {
            return feature.properties.status == 2;
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                icon: processIcon
            }).on('mouseover', function () {
                this.bindPopup(feature.properties.description).openPopup();
            });
        }
    });
    var completedComplaints = L.geoJson(data, {
        onEachFeature: onEachFeature,
        filter: function (feature, layer) {
            return feature.properties.status == 4;
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                icon: completedIcon
            }).on('mouseover', function () {
                this.bindPopup(feature.properties.description).openPopup();
            });
        }
    });
    var canceledComplaints = L.geoJson(data, {
        onEachFeature: onEachFeature,
        filter: function (feature, layer) {
            return feature.properties.status == 5;
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                icon: cancelIcon
            }).on('mouseover', function () {
                this.bindPopup(feature.properties.description).openPopup();
            });
        }
    });
    var readyComplaints = L.geoJson(data, {
        onEachFeature: onEachFeature,
        filter: function (feature, layer) {
            return feature.properties.status == 3;
        },
        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {
                icon: readyIcon
            }).on('mouseover', function () {
                this.bindPopup(feature.properties.description).openPopup();
            });
        }
    });
    //mymap.fitBounds(allComplaints.getBounds(), {
    //    padding: [50, 50]
    //});
    newComplaints.addTo(mymap)
    processComplaints.addTo(mymap)
    completedComplaints.addTo(mymap)
    canceledComplaints.addTo(mymap)
    readyComplaints.addTo(mymap)

    // The JavaScript below is new
    $("#newComplaints").click(function () {
        mymap.addLayer(newComplaints)
        mymap.removeLayer(processComplaints)
        mymap.removeLayer(canceledComplaints)
        mymap.removeLayer(readyComplaints)
        mymap.removeLayer(completedComplaints)
    });
    $("#processComplaints").click(function () {
        mymap.removeLayer(newComplaints)
        mymap.addLayer(processComplaints)
        mymap.removeLayer(canceledComplaints)
        mymap.removeLayer(readyComplaints)
        mymap.removeLayer(completedComplaints)
    });
    $("#completedComplaints").click(function () {
        mymap.removeLayer(newComplaints)
        mymap.removeLayer(processComplaints)
        mymap.removeLayer(canceledComplaints)
        mymap.removeLayer(readyComplaints)
        mymap.addLayer(completedComplaints)
    });
    $("#canceledComplaints").click(function () {
        mymap.removeLayer(newComplaints)
        mymap.removeLayer(processComplaints)
        mymap.addLayer(canceledComplaints)
        mymap.removeLayer(readyComplaints)
        mymap.removeLayer(completedComplaints)
    });
    $("#readyComplaints").click(function () {
        mymap.removeLayer(newComplaints)
        mymap.removeLayer(processComplaints)
        mymap.removeLayer(canceledComplaints)
        mymap.addLayer(readyComplaints)
        mymap.removeLayer(completedComplaints)
    });
    $("#allComplaints").click(function () {
        mymap.addLayer(newComplaints)
        mymap.addLayer(processComplaints)
        mymap.addLayer(canceledComplaints)
        mymap.addLayer(readyComplaints)
        mymap.addLayer(completedComplaints)
    });
});

var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create("div", "info");
    this.update();
    return this._div;
};

info.update = function (props, geometry) {
    if (props) {
        let numCallbackRuns = 0;
        jQuery.each(allComplaints, function (i, element) {
            var coors = geometry ? geometry.coordinates[0] : null;
            var isInside = inside(element.geometry.coordinates, coors);
            if (isInside) {
                numCallbackRuns++;
            }
        });
        this._div.innerHTML =
            (props
                ? "<b>" +
                props.name +
                "</b><br /> Количество заявок: " +
                numCallbackRuns : "Наведите на район");
    }
    else {
        this._div.innerHTML = "Наведите на район";
    }
};

info.addTo(mymap);

function getColor(color) {
    return color;
}

function style(feature) {
    return {
        fillColor: getColor(feature.properties.color),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: "#666",
        dashArray: "",
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    info.update(layer.feature.properties, layer.feature.geometry);
}

var geoBishkek;

function resetHighlight(e) {
    geoBishkek.resetStyle(e.target);
    info.update();
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature2(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

promise2.then(function (data) {
    console.log(data);
    geoBishkek = L.geoJson(data, {
        style: style,
        onEachFeature: onEachFeature2
    });
    geoBishkek.addTo(mymap)
});