﻿$(document).ready(function () {

    $('.footable').footable();

    $('.delete-alert').click(function () {

        let elem = $(this);
        let item_id = elem.data('itemid');

        swal({
            title: "Вы уверены?",
            text: "Выбранная запись будет удалена из системы!",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Отмена",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Да, удалить запись!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type: 'POST',
                url: 'StructuralUnit/Delete',
                dataType: 'json',
                data: {
                    Id: item_id
                },
                statusCode: {
                    200: function () {
                        let row = elem.parents('tr');
                        row.remove();

                        swal("Удалено!", "Запись успешно удалена.", "success");
                    },
                    400: function () {
                        swal("Не удалено!", "Возникли ошибки во время удаления записи.", "error");
                    }
                }
            });
        });

    });

});