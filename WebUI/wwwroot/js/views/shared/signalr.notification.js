﻿$(document).ready(function () {
    const hubConnection = new signalR.HubConnectionBuilder().withUrl("https://localhost:44348/hubs/notification").build();

    hubConnection.on('NewComplaintNotify', function () {

        let total_notification_count = $('#total_notification').text();
        $('#total_notification').text(++total_notification_count);

        let new_complaints_notification_count = $('#new_complaints_notification').text();
        $('#new_complaints_notification').text(++new_complaints_notification_count);

    });

    hubConnection.start();
});