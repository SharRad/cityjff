﻿using Application.MediatR.StructuralUnits.Commands.CreateStructuralUnit;
using Application.MediatR.StructuralUnits.Commands.DeleteStructuralUnit;
using Application.MediatR.StructuralUnits.Commands.EditStructuralUnit;
using Application.MediatR.StructuralUnits.Queries.GetStructuralUnits;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class StructuralUnitController : BaseController
    {
        public async Task<IActionResult> Index()
        {
            var StructuralUnits = await Mediator.Send(new GetStructuralUnitsQuery());
            return View(StructuralUnits);
        }

        public IActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateStructuralUnitCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, result.Errors.ToString());
                return View(command);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int id)
        {
            var StructuralUnit = await Mediator.Send(new GetStructuralUnitByIdQuery { Id = id });

            var editStructuralUnitCommand = new EditStructuralUnitCommand
            {
                Id = StructuralUnit.Id,
                Name = StructuralUnit.Name
            };

            return View(editStructuralUnitCommand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditStructuralUnitCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, result.Errors.ToString());
                return View(command);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteStructuralUnitCommand command)
        {
            var result = await Mediator.Send(command);

            if (!result.Succeeded)
                return BadRequest(result.Errors.ToString());

            return Ok();
        }
    }
}