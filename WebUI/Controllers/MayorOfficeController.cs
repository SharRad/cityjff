﻿using Application.MediatR.MayorOffices.Commands.CreateMayorOffice;
using Application.MediatR.MayorOffices.Commands.DeleteMayorOffice;
using Application.MediatR.MayorOffices.Commands.EditMayorOffice;
using Application.MediatR.MayorOffices.Queries.GetMayorOffices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class MayorOfficeController : BaseController
    {
        public async Task<IActionResult> Index()
        {
            var mayorOffices = await Mediator.Send(new GetMayorOfficesQuery());
            return View(mayorOffices);
        }

        public IActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateMayorOfficeCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, result.Errors.ToString());
                return View(command);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int id)
        {
            var mayorOffice = await Mediator.Send(new GetMayorOfficeByIdQuery { Id = id });

            var editMayorOfficeCommand = new EditMayorOfficeCommand
            {
                Id = mayorOffice.Id,
                Name = mayorOffice.Name
            };

            return View(editMayorOfficeCommand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditMayorOfficeCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, result.Errors.ToString());
                return View(command);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteMayorOfficeCommand command)
        {
            var result = await Mediator.Send(command);

            if (!result.Succeeded)
                return BadRequest(result.Errors.ToString());

            return Ok();
        }
    }
}