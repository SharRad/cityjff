﻿using Application.MediatR.Maps.Queries.GetStructureMapComplaint;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class MapController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> LoadMapComplaint()
        {
            var mapComplaint = await Mediator.Send(new GetStructureMapComplaintQuery());
            return Ok(mapComplaint);
        }

        public IActionResult LoadGeoBishkek()
        {
            var geoJson = System.IO.File.ReadAllText("geojson-bishkek.json");
            return Ok(geoJson);
        }
    }
}