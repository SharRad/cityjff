﻿using Application.MediatR.Accounts.Commands.BanAccount;
using Application.MediatR.Accounts.Commands.UnbanAccount;
using Application.MediatR.Managers.Commands.CreateManagerAccount;
using Application.MediatR.Managers.Queries.GetManagers;
using Application.MediatR.StructuralUnits.Queries.GetStructuralUnits;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManagerController : BaseController
    {
        public async Task<IActionResult> Index()
        {
            var managers = await Mediator.Send(new GetManagersQuery());
            return View(managers);
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.StructuralUnits = new SelectList(await Mediator.Send(new GetStructuralUnitsQuery()), "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateManagerAccountCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = await Mediator.Send(command);
                TempData["IsUpdated"] = result.Succeeded;
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Ban(int id)
        {
            var result = await Mediator.Send(new BanAccountCommand { Id = id });
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Unban(int id)
        {
            var result = await Mediator.Send(new UnbanAccountCommand { Id = id });
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }
    }
}