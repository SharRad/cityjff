﻿using Application.MediatR.CitizenComplaints.Commands.EditCitizenComplaint;
using Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints;
using Application.MediatR.DivisionCitizenComplaints.Commands.CreateDivisionComplaint;
using Application.MediatR.DivisionCitizenComplaints.Commands.ResendDivisionComplaint;
using Application.MediatR.StructuralUnits.Queries.GetStructuralUnits;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    public class CitizenComplaintController : BaseController
    {
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Index(int page = 1)
        {
            var citizenComplaints = await Mediator.Send(new GetCitizenComplaintsQuery { Page = page });
            return View(citizenComplaints);
        }

        [Authorize(Roles = "Administrator, Manager")]
        public async Task<IActionResult> Details(int id)
        {
            ViewBag.StructuralUnits = new SelectList(await Mediator.Send(new GetStructuralUnitsQuery()), "Id", "Name");
            var citizenComplaint = await Mediator.Send(new GetCitizenComplaintByIdQuery { Id = id });
            return View(citizenComplaint);
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Send(int id)
        {
            ViewBag.StructuralUnits = new SelectList(await Mediator.Send(new GetStructuralUnitsQuery()), "Id", "Name");
            return View(id);
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Resend(ResendDivisionComplaintCommand command)
        {
            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Details), new { id = command.ComplaintId });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<IActionResult> Send(CreateDivisionComplaintCommand command)
        {
            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<IActionResult> Cancel(EditCitizenComplaintStatusCommand command)
        {
            command.Status = CitizenComplaintStatus.Canceled;
            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public async Task<IActionResult> Complete(EditCitizenComplaintStatusCommand command)
        {
            command.Status = CitizenComplaintStatus.Completed;
            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }
    }
}