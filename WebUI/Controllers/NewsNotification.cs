﻿using Application.MediatR.NewsNotifications.Commands.CreateNewsNotification;
using Application.MediatR.NewsNotifications.Commands.DeleteNewsNotification;
using Application.MediatR.NewsNotifications.Commands.EditNewsNotification;
using Application.MediatR.NewsNotifications.Queries.GetNewsNotifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    public class NewsNotification : BaseController
    {
        public async Task<IActionResult> Index(int page = 1)
        {
            var NewsNotifications = await Mediator.Send(new GetUserNewsNotificationsQuery { Page = page });
            return View(NewsNotifications);
        }

        public IActionResult Create() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateNewsNotificationCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, result.Errors.ToString());
                return View(command);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int id)
        {
            var NewsNotification = await Mediator.Send(new GetNewsNotificationByIdQuery { Id = id });

            var editNewsNotificationCommand = new EditNewsNotificationCommand
            {
                Id = NewsNotification.Id,
                Title = NewsNotification.Title,
                MessageBody = NewsNotification.MessageBody,
                Caption = NewsNotification.Caption
            };

            return View(editNewsNotificationCommand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditNewsNotificationCommand command)
        {
            if (!ModelState.IsValid)
                return View(command);

            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;

            if (!result.Succeeded)
            {
                ModelState.AddModelError(string.Empty, result.Errors.ToString());
                return View(command);
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(DeleteNewsNotificationCommand command)
        {
            var result = await Mediator.Send(command);

            if (!result.Succeeded)
                return BadRequest(result.Errors.ToString());

            return Ok();
        }
    }
}