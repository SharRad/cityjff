﻿using Application.MediatR.ComplaintNotifications.Commands.DeleteComplaintNotification;
using Application.MediatR.ComplaintNotifications.Queries.GetComplaintNotificationsQuery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    public class ComplaintNotificationController : BaseController
    {
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Index()
        {
            var complaintNotifications = await Mediator.Send(new GetComplaintNotificationsQuery());

            return View(complaintNotifications);
        }


        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AdminCitizenComplaintNotifications()
        {
            await Mediator.Send(new DeleteAdminNewComplaintNotificationsCommand());

            return RedirectToAction("Index", "CitizenComplaint");
        }

        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> ManagerCitizenComplaintNotifications()
        {
            await Mediator.Send(new DeleteManagerNewComplaintNotificationsCommand());

            return RedirectToAction("Index", "DivisionComplaint");
        }


        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Details(int id, int complaintId)
        {
            await Mediator.Send(new DeleteComplaintNotificationCommand { Id = id });

            return RedirectToAction("Details", "CitizenComplaint", new { id = complaintId });
        }
    }
}
