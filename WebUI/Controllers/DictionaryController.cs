﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class DictionaryController : BaseController
    {
        public IActionResult Index() => View();
    }
}