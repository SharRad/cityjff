﻿using Application.MediatR.Admins.Commands.CreateAdminAccount;
using Application.MediatR.Admins.Queries.GetAdmins;
using Application.MediatR.MayorOffices.Queries.GetMayorOffices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministratorController : BaseController
    {
        public async Task<IActionResult> Index()
        {
            var admins = await Mediator.Send(new GetAdminsQuery());
            return View(admins);
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.MayorOffices = new SelectList(await Mediator.Send(new GetMayorOfficesQuery()), "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateAdminAccountCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = await Mediator.Send(command);
                TempData["IsConfirmed"] = result.Succeeded;
            }

            return RedirectToAction(nameof(Index));
        }
    }
}