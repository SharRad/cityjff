﻿using Application.MediatR.DivisionCitizenComplaints.Commands.CancelDivisionComplaint;
using Application.MediatR.DivisionCitizenComplaints.Commands.CompleteDivivsionComplaint;
using Application.MediatR.DivisionCitizenComplaints.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Manager")]
    public class DivisionComplaintController : BaseController
    {
        public async Task<IActionResult> Index(int page = 1)
        {
            var citizenComplaints = await Mediator.Send(new GetDivisionCitizenComplaintsQuery { Page = page });
            return View(citizenComplaints);
        }

        [HttpPost]
        public async Task<IActionResult> Cancel(CancelDivisionComplaintCommand command)
        {
            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Complete(CompleteDivisionComplaintCommand command)
        {
            var result = await Mediator.Send(command);
            TempData["IsUpdated"] = result.Succeeded;
            return RedirectToAction(nameof(Index));
        }
    }
}