﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public IActionResult Index() => View();
    }
}