﻿using Application.Common.Interfaces;
using Application.MediatR.Accounts.Commands.ChangePassword;
using Application.MediatR.Accounts.Commands.ForgotPassword;
using Application.MediatR.Accounts.Commands.Login;
using Application.MediatR.Accounts.Commands.SendResetPasswordEmail;
using Application.MediatR.Accounts.Commands.SignOut;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginCommand command, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var authResult = await Mediator.Send(command);

                if (authResult.Succeeded)
                {
                    if (!String.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    else
                        return RedirectToAction("Index", "Home");
                }
                else
                {
                    for (int i = 0; i < authResult.Errors.Length; i++)
                    {
                        ModelState.AddModelError(String.Empty, authResult.Errors[i]);
                    }
                }
            }

            return View(command);
        }

        public IActionResult ChangePassword([FromServices] IHttpContextUserService userService)
        {
            ChangePasswordCommand model = new ChangePasswordCommand { UserId = userService.UserId };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = await Mediator.Send(command);
                if (result.Succeeded)
                {
                    TempData["IsUpdated"] = result.Succeeded;
                    return RedirectToAction("Index", "Home");
                }
            }

            TempData["IsUpdated"] = false;
            return View(command);
        }

        [AllowAnonymous]
        public IActionResult ForgotPassword() => View();

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(SendResetPasswordEmailCommand command)
        {
            var result = await Mediator.Send(command);
            TempData["IsConfirmed"] = result.Succeeded;
            return RedirectToAction(nameof(Login));
        }

        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ForgotPasswordCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = await Mediator.Send(command);
                TempData["IsConfirmed"] = result.Succeeded;
            }

            return RedirectToAction(nameof(Login));
        }

        public async Task<IActionResult> Logout()
        {
            await Mediator.Send(new SignOutCommand());
            return RedirectToAction(nameof(Login));
        }
    }
}