﻿using Application.Common.Models;
using Application.MediatR.NewsNotifications.Commands.UpdateNotificationEnableStatus;
using Application.MediatR.NewsNotifications.Queries.GetNewsNotifications;
using Application.MediatR.NewsNotifications.Queries.GetNotificationEnableStatus;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using P.Pager;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class NewsNotificationController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(typeof(IPager<NewsNotificationDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int page = 1)
        {
            var newsNotifications = await Mediator.Send(new GetNewsNotificationsQuery { Page = page });
            return Ok(newsNotifications);
        }

        [HttpPost("notification-enable")]
        [ProducesResponseType(typeof(Result), StatusCodes.Status200OK)]
        public async Task<IActionResult> EnableNotifications(string token, bool enable)
        {
            var status = await Mediator.Send(new UpdateNotificationEnableStatusCommand { Token = token, Enable = enable });
            return Ok(status);
        }

        [HttpGet("notification-enable-status")]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetNotificationEnableStatus(string token)
        {
            var status = await Mediator.Send(new GetNotificationEnableStatusQuery { Token = token });
            return Ok(status);
        }
    }
}