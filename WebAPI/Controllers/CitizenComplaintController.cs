﻿using Application.Common.DTOs;
using Application.Common.Models;
using Application.MediatR.CitizenComplaints.Commands.CreateCitizenComplaint;
using Application.MediatR.CitizenComplaints.Queries.GetCitizenComplaints;
using Application.MediatR.ComplaintNotifications.Commands.CreateComplaintNotification;
using Application.MediatR.Users.Queries.GetUsers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Hubs;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class CitizenComplaintController : BaseController
    {
        private readonly IHubContext<NotificationHub> _hubContext;

        public CitizenComplaintController(IHubContext<NotificationHub> hubContext)
        {
            _hubContext = hubContext;
        }


        [HttpGet]
        [ProducesResponseType(typeof(List<CitizenComplaintShortDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            var citizenComplaints = await Mediator.Send(new GetCitizenComplaintsWithoutNewQuery());

            return Ok(citizenComplaints);
        }


        [HttpGet("user-complaints")]
        [ProducesResponseType(typeof(List<CitizenComplaintShortDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserComplaints()
        {
            var citizenComplaints = await Mediator.Send(new GetUserCitizenComplaintsQuery());

            return Ok(citizenComplaints);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CitizenComplaintDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int id)
        {
            var citizenComplaint = await Mediator.Send(new GetCitizenComplaintByIdQuery { Id = id });

            return Ok(citizenComplaint);
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromForm] CreateCitizenComplaintCommand command)
        {
            (Result result, int citizenComplaintId) = await Mediator.Send(command);

            if (!result.Succeeded)
                return BadRequest(result.Errors);

            int mayorOfficeId = 1;

            await Mediator.Send(new CreateNewComplaintNotificationCommand
            {
                MayorOfficeId = mayorOfficeId,
                CitizentComplaintId = citizenComplaintId
            });


            //var mayorOfficeUsersIds = await Mediator.Send(new GetMayorOfficeUsersIdQuery { MayorOfficeId = mayorOfficeId });

            //await _hubContext.Clients.Users(mayorOfficeUsersIds).SendAsync("NewComplaintNotify");

            await _hubContext.Clients.All.SendAsync("NewComplaintNotify");
            
            return Ok();
        }
    }
}