﻿using Application.Common.DTOs;
using Application.Common.Models;
using Application.MediatR.Tokens.Commands.Authenticate;
using Application.MediatR.Tokens.Commands.RefreshToken;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    /// <summary>
    /// контроллер для работы с токеном авторизации
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TokenController : BaseController
    {
        /// <summary>
        /// Получение токена пользователя. (Signin юзера в системе)
        /// </summary>
        [HttpPost]
        [ProducesResponseType(typeof(ApiUserToken), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status400BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] AuthenticateCommand command)
        {
            (Result Result, ApiUserToken User) = await Mediator.Send(command);

            if (Result.Succeeded)
                return Ok(User);

            return BadRequest(Result.Errors);
        }

        /// <summary>
        /// Обновление jwt токена
        /// </summary>
        /// <param name="token"></param>
        [HttpPost("refresh")]
        [ProducesResponseType(typeof(ApiUserToken), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status400BadRequest)]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] RefreshTokenCommand command)
        {
            (Result Result, ApiUserToken Token) = await Mediator.Send(command);

            if (Result.Succeeded)
                return Ok(Token);

            return BadRequest(Result.Errors);
        }
    }
}