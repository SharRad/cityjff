﻿using Application.MediatR.Accounts.Commands.ChangePassword;
using Application.MediatR.Accounts.Commands.SignOut;
using Application.MediatR.Users.Commands.CreateUserAccount;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class AccountController : BaseController
    {
        /// <summary>
        /// Изменение пароля пользователя
        /// Доступ разрешен: Все роли
        /// </summary>
        [HttpPost("changepwd")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordCommand command)
        {
            var result = await Mediator.Send(command);
            if (result.Succeeded)
                return Ok();
            return BadRequest(result.Errors);
        }


        /// <summary>
        /// Выход из системы
        /// Доступ разрешен: Все роли
        /// </summary>
        [HttpPost("signout")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(SignOutCommand command)
        {
            var result = await Mediator.Send(command);

            if (result.Succeeded)
                return Ok();

            return BadRequest(result.Errors);
        }

        /// <summary>
        /// Регистрация юзера
        /// </summary>
        [HttpPost("register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post(CreateUserAccountCommand command)
        {
            var result = await Mediator.Send(command);

            if (result.Succeeded)
                return Ok();

            return BadRequest(result.Errors);
        }
    }
}
