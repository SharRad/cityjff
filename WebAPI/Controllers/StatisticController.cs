﻿using Application.MediatR.Statistics.Queries.GetCitizenComplaintsByMonthly;
using Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaints;
using Application.MediatR.Statistics.Queries.GetFinishedCitizenComplaintsByPercent;
using Application.MediatR.Statistics.Queries.GetTotalCitizenComplaints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class StatisticController : BaseController
    {
        [HttpGet("total-complaints")]
        [ProducesResponseType(typeof(TotalCitizenComplaintsDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetTotal()
        {
            var totalCitizenComplaints = await Mediator.Send(new GetTotalCitizenComplaintsQuery());

            return Ok(totalCitizenComplaints);
        }

        [HttpGet("monthly-complaints")]
        [ProducesResponseType(typeof(MonthlyCitizenComplaintsDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMonthly()
        {
            var citizenComplaintsByMonthes = await Mediator.Send(new GetCitizenComplaintsByMonthlyQuery());

            return Ok(citizenComplaintsByMonthes);
        }

        [HttpGet("finished-complaints")]
        [ProducesResponseType(typeof(FinishedCitizenComplaintsDto), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFinished()
        {
            var finishedCitizenComplaints = await Mediator.Send(new GetFinishedCitizenComplaintsQuery());

            return Ok(finishedCitizenComplaints);
        }

        [HttpGet("finished-percentage-complaints")]
        [ProducesResponseType(typeof(double), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFinishedPercentage()
        {
            double finishedPercentComplaints = await Mediator.Send(new GetFinishedCitizenComplaintsByPercentQuery());

            return Ok(finishedPercentComplaints);
        }
    }
}